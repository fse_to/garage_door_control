
PROGRAM _INIT
	
	this ACCESS ADR(gAlarm); 
	
END_PROGRAM

PROGRAM _CYCLIC
	
	this ACCESS ADR(gAlarm);
	
	this.alarms[0] := NOT gMain.io.din.Pos4FB; 					// Main Motor cut off
	this.alarms[1] := FALSE; 
	this.alarms[2] := NOT gMain.io.din.LevelandTempOK; 					// Oil level or temperature not ok
	this.alarms[3] := gMain.io.din.SafetyContactor; 			// eStop
	this.alarms[4] := (gMain.status.state = STATE_SAFETY_STOP); // In Safety stop state, needs reset. 
	this.alarms[5] := NOT(gMain.io.ain.P1.status =0) ; // P1 Sensor Error
	this.alarms[6] := NOT(gMain.io.ain.Pos17.status =0) ; // Pos17 Sensor Error
	this.alarms[7] := NOT(gMain.io.ain.Pos228.status =0) ; // Pos228 Sensor Error
	
	
	FOR i := 0 TO MAX_ALARMS DO 
		IF this.alarms[i] THEN
			this.alarmActive := TRUE;
			this.hideAlarm.0 := 0; 
			EXIT; 
		END_IF
		this.alarmActive := FALSE; 
		this.hideAlarm.0 := 1; 
	END_FOR
			
	// Show/Hide reset button. This is not necessary, when physical safety reset button is implemented. 
	IF this.alarms[4] THEN 
		this.hideReset.0 := 0; 
	ELSE
		this.hideReset.0 := 1; 
	END_IF
	
			
END_PROGRAM

PROGRAM _EXIT
	 
END_PROGRAM

