
TYPE
	BRSE_ARL_Arguments_typ : 	STRUCT 
		r : ARRAY[0..1] OF REAL;
		s : ARRAY[0..6] OF UDINT;
		b : ARRAY[0..0] OF BOOL; (*Not used*)
		i : ARRAY[0..0] OF DINT;
	END_STRUCT;
END_TYPE
