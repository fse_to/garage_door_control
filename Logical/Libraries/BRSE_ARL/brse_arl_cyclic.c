/*! \file brse_arl_cyclic.c */

#include "brse_arl_main.h"

/*! \interface BRSE_ARL_Cyclic
	\brief Cyclic call to flush pending entries to the logbook
	
	This function writes pending entries to the logbook and has to be called cyclically.
	
	\param ID Logger ID of the logbook to be written to
    \retval 0 all messages written to logger
	\retval 65535 pending entries to be written
	\retval Error Error code from the AsArLog library
*/
unsigned short BRSE_ARL_Cyclic(unsigned short ID)
{
	BRSE_ARL_Logger_typ* pLogger;
	BRSE_ARL_LogbookEntry_typ* pCurEntry;
	UINT status;
	
	/* Check if ID is valid */
	if ((ID >= BRSE_ARL_MAX_LOGGERS) 
		|| (BRSE_ARL_LoggerList[ID] == NULL)
		|| (BRSE_ARL_LoggerList[ID]->ID != (BRSE_ARL_ID_BASE | (unsigned long)ID)))
	{
		return BRSE_ARL_ID_NOT_VALID;
	}

	/* Access the chosen logger */
	pLogger = BRSE_ARL_LoggerList[ID];

	switch (pLogger->Cyclic_Step)
	{
		/* OPEN LOGBOOK */
		case 0:
			/* If a name is not given, take the default user logbook */
			if (pLogger->LogbookName[0] == '\0')
			{
				pLogger->LogbookIdent = arlogBASE_USER_LOG_IDENT;
				pLogger->Cyclic_Step = 10;
				status = ERR_FUB_BUSY;
				break;
			}
			/* Otherwise fall through to next step */
			pLogger->Cyclic_Step = 1;
						
		/* GET LOGBOOK */
		case 1:
			/* Check if a logbook with the given name is existing */
			pLogger->FB_AsArLogGetInfo.enable = TRUE;
			pLogger->FB_AsArLogGetInfo.pName = (UDINT)pLogger->LogbookName;
			
			AsArLogGetInfo(&pLogger->FB_AsArLogGetInfo);

			switch (pLogger->FB_AsArLogGetInfo.status)
			{
				/* ERR_OK */
				case ERR_OK:
					/* Save the ident of the exisiting logbook */
					pLogger->LogbookIdent = pLogger->FB_AsArLogGetInfo.ident;
					
					/* Go to operating state */
					pLogger->Cyclic_Step = 10;
					status = ERR_FUB_BUSY;
					break;

				/* arlogERR_NOTEXISTING */
				case 31416:
					/* Create a new logbook with the given name and save the ident */
					pLogger->Cyclic_Step = 2;
					status = ERR_FUB_BUSY;
					break;
			
				/* ERR_FUB_BUSY */		
				case ERR_FUB_BUSY:
					status = ERR_FUB_BUSY;
					break;
					
				default:
					status = pLogger->FB_AsArLogGetInfo.status;
					break;
			}
			break;
			
		/* CREATE LOGBOOK */
		case 2:
			/* Create a new logbook in USRROM */
			pLogger->FB_AsArLogCreate.enable = TRUE;
			pLogger->FB_AsArLogCreate.pName = (UDINT)pLogger->LogbookName;
			pLogger->FB_AsArLogCreate.len = pLogger->LogbookSize;
			pLogger->FB_AsArLogCreate.memType = arlogUSRROM;
			
			AsArLogCreate(&pLogger->FB_AsArLogCreate);

			switch (pLogger->FB_AsArLogCreate.status)
			{
				/* ERR_OK */
				case ERR_OK:
					/* Set the ident of the newly created logbook */
					pLogger->LogbookIdent = pLogger->FB_AsArLogCreate.ident;
					
					/* Go to operating state */
					pLogger->Cyclic_Step = 10;
					status = ERR_FUB_BUSY;
					break;
					
				/* ERR_FUB_BUSY */
				case ERR_FUB_BUSY:
					status = ERR_FUB_BUSY;
					break;
					
				default:
					status = pLogger->FB_AsArLogCreate.status;
					break;
			}
			break;
			
		/* POP ENTRY FROM THE FIFO AND WRITE IT TO THE LOGBOOK */
		case 10:
			if (pLogger->CurBufEntries == 0)
			{
				/* No entry in the fifo */
				/* Answer with OK then... */
				status = ERR_OK;
			}
			else if (!pLogger->fifo_write_lock)
			{
				/* Got an entry in the fifo */
				pCurEntry = (BRSE_ARL_LogbookEntry_typ*)((void*)pLogger->Buffer + pLogger->EntrySize * pLogger->ReadIndex);
				
				pLogger->FB_AsArLogWrite.enable = TRUE;
				pLogger->FB_AsArLogWrite.ident = pLogger->LogbookIdent;
				pLogger->FB_AsArLogWrite.logLevel = pCurEntry->LogLevel;
				pLogger->FB_AsArLogWrite.errornr = pCurEntry->ErrorNo;
				pLogger->FB_AsArLogWrite.mem = (UDINT)NULL;
				pLogger->FB_AsArLogWrite.len = 0;
				pLogger->FB_AsArLogWrite.asciiString = (UDINT)pCurEntry->ErrorMsg;
	
				AsArLogWrite(&pLogger->FB_AsArLogWrite);
				
				switch (pLogger->FB_AsArLogWrite.status)
				{
					/* ERR_OK */
					case ERR_OK:
						/* Adjust the FIFO:
						 * Increase the ReadIndex, but reset it to 0 if MaxBufEntries have been exceeded
						 */
						pLogger->ReadIndex++;
						if (pLogger->ReadIndex >= pLogger->MaxBufEntries)
						{
							pLogger->ReadIndex = 0;
						}
						pLogger->CurBufEntries--;
					
						/* Return OK if no more entries shall be written */
						status = (pLogger->CurBufEntries == 0) ? ERR_OK : ERR_FUB_BUSY;
						break;
						
					/* ERR_FUB_BUSY */
					case ERR_FUB_BUSY:
						status = ERR_FUB_BUSY;
						break;
						
					/* Other error */
					default:
						status = pLogger->FB_AsArLogWrite.status;
						break;
				}
			}
			else
			{
				/* Write is active */
				status = ERR_FUB_BUSY;
			}
			break;
	}
	
	return status;
}
