/*! \file version_history.c */

/*! \page History
\section History
\par V1.00
\arg Implemented a buffered logbook writer
\par V1.01
\arg Splitted the source file into several files
\arg renamed MsgData to Arguments, changed the String datatype to UDINT (string pointer)
\par V1.02
\arg added documentation in source files.
\arg Changed Arguments to Arguments_typ
\par V1.03
\arg Removed dependency on RtkLib
\arg Several buffered logbooks can be used (identified by a unique ID)
\par V1.04
\arg Version created for AS3.0
\par V1.05
\arg Using mystring.h instead of string.h to avoid -lc build options in the phtysical view.
\par V1.06
\arg The problem with string.h has been solved in runtime versions >= 2.95, so string.h is used instead of mystring.
*/
