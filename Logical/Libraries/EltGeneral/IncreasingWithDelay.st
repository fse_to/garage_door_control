(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: EltGeneral
 * File: LevelSteps.st
 * Author: villefrancea
 * Created: June 22, 2016
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

(* TODO: Add your comment here *)
FUNCTION_BLOCK IncreasingWithDelay
	
	levelOld := levelOut;
	IF levelNew > levelOld  AND TOF_SWITCHDELAY.Q = FALSE THEN
		levelOut := levelOld+1;
		TOF_SWITCHDELAY.IN:=TRUE;
	ELSIF levelNew <= levelOld THEN // reduce level if necessary
		levelOut := levelNew;
		TOF_SWITCHDELAY.IN :=FALSE;
	ELSE
		TOF_SWITCHDELAY.IN :=FALSE;
		levelOut := levelOld;
	END_IF
	TOF_SWITCHDELAY(PT:=DelayTime);
	
END_FUNCTION_BLOCK
