(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Library: EltGeneral
 * File: PRESSURE.st
 * Author: avi
 * Created: September 21, 2011
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

(* Converts AI pressure inputs to Real value in Bar *)
FUNCTION_BLOCK TEMP
	
	// Pressure sensor. Input signal in the range 4...20mA -> 0...32767 -> 0.... XXX(par)
	IF hw=32767 THEN
		v := 999.99;
		status := 1;
	ELSIF hw<-1000 THEN
		v := -9.99;
		status := 2; 
	ELSE
		
		v := (INT_TO_REAL(hw) / 32767.0 * (scale20mA-scale4mA))+scale4mA;
		status := ERR_OK;
	END_IF
	IF v<0 THEN
		v:=0;
	END_IF
END_FUNCTION_BLOCK