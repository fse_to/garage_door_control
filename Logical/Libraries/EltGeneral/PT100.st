(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Library: EltGeneral
 * File: PT100.st
 * Author: avi
 * Created: September 21, 2011
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

(* Converts PT100 inputs to Real value in �C *)
FUNCTION_BLOCK PT100
	IF hw=32767 THEN
		v := 999.99;
		status := 1;
	ELSIF hw<=-32767 THEN
		v := -999.99;
		status := 2; 
	ELSE
		v := hw * 0.1;
		status := ERR_OK;
	END_IF
END_FUNCTION_BLOCK