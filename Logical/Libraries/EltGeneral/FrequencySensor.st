
(* TODO: Add your comment here *)
FUNCTION_BLOCK FrequencySensor
	t_log_old := t_log;
	DTStructureGetTime_1(enable := 1, pDTStructure := ADR(t_log));	
	
	T := ((UINT_TO_REAL(t_log.millisec)+UINT_TO_REAL(t_log.microsec)/1000)-(UINT_TO_REAL(t_log_old.millisec)+UINT_TO_REAL(t_log_old.microsec)/1000))/1000;
	
	IF T<0 THEN
		T := (1000 + (UINT_TO_REAL(t_log.millisec)+UINT_TO_REAL(t_log.microsec)/1000)-(UINT_TO_REAL(t_log_old.millisec)+UINT_TO_REAL(t_log_old.microsec)/1000))/1000;
	END_IF
	
	CASE state OF
		0:
			count := 0;
			IF EDGEPOS(in) THEN
				state := 1;
			END_IF
		1:
			count := count +1;
			IF EDGEPOS(in) THEN
				out := count*T;
				state := 0;
			END_IF	 
		
	
	END_CASE;	
	
	//out := out*0.005;
	//out_freq := 1.0/(UINT_TO_REAL(count)*dt)
	//slope := (max_y - min_y)/(min_freq - max_freq);
	
	
	//out := elapsed_time;
	//out := slope*(TIME_TO_REAL(timer.ET)/1000.0 - min_y) + min_freq;
	
	//timer();
END_FUNCTION_BLOCK
