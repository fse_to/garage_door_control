(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Library: EltGeneral
 * File: PRESSURE.st
 * Author: avi
 * Created: September 21, 2011
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

(* Converts serial number of PLC to system identifier*)
FUNCTION_BLOCK IdentifySystem
	
	IF SerialNumber = 174610 THEN
		GW1 := FALSE; 
		GW2 := TRUE; 
		GW3 := FALSE; 
		TEST_RIG_PITCH := FALSE; 
		TEST_RIG_LOAD := FALSE;
		GW0_series := FALSE;
	ELSIF SerialNumber = 173694 THEN
		GW1 := TRUE; 
		GW2 := FALSE; 
		GW3 := FALSE; 
		TEST_RIG_PITCH := FALSE; 
		TEST_RIG_LOAD := FALSE;
		GW0_series := FALSE;
	ELSIF SerialNumber = 185268 THEN
		GW1 := FALSE; 
		GW2 := FALSE; 
		GW3 := TRUE; 
		TEST_RIG_PITCH := FALSE; 
		TEST_RIG_LOAD := FALSE;
		GW0_series := FALSE;
	ELSE
		// TO DO: Register serial number of GW test rig PLCs
		GW1 := FALSE; 
		GW2 := FALSE; 
		GW3 := FALSE; 
		TEST_RIG_PITCH := FALSE; 
		TEST_RIG_LOAD := FALSE;
		GW0_series := TRUE;
	END_IF
END_FUNCTION_BLOCK