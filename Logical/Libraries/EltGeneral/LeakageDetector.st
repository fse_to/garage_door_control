(********************************************************************
 * COPYRIGHT -- Fritz Schur
 ********************************************************************
 * Library: EltGeneral
 * File: LeakageDetector.st
 * Author: vie
 * Created: September 26, 2018
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

(* TODO: Add your comment here *)
FUNCTION_BLOCK LeakageDetector

	IF Enable THEN
		
		//Indexing of filters is [Slow, Fast, VeryFast]
		IF Initialize OR NOT(Initialized) THEN 
			
			AccumulatedInput :=AccumulatedInput +  SystemOilMass;
			InitSamples := InitSamples + 1;
			
			AvgInitValue := AccumulatedInput/InitSamples;
			
			Initialized := FALSE;
			// Initialize Filters (Maybe we do  not need the initialize variable)	
			ReferenceFilter(enable := TRUE, V := 1, T1 := ReferenceFilterTime, Set_y := 1, y_set :=  AvgInitValue, u := AvgInitValue);
			FOR i:=0 TO 2 DO 
				LeakageFilters[i](enable := TRUE, V := 1, T1 := LeakageFilterTimes[i], Set_y := 1, y_set :=  AvgInitValue, u := AvgInitValue);
				LeakageAccumulator[i] := 0.0;
				TON_LeakageAlarmTimer[i].PT := T#7s;
			END_FOR
			
			TON_EvaluationTimer.PT := T#600s;
			TON_EvaluationTimer.IN := TRUE;
			
			IF TON_EvaluationTimer.Q = TRUE THEN
				// Return to the normal operation of the filters and set the initialized variable to true
				Initialized := TRUE;
				AccumulatedInput :=0.0;
				InitSamples := 0.0;
				AvgInitValue := 0.0;
				ReferenceFilter.Set_y := 0;
				TON_EvaluationTimer.IN := FALSE;
				FOR i:=0 TO 2 DO 
					LeakageFilters[i](Set_y := 0);
				END_FOR
			END_IF
			
		ELSE
			
			IF NOT(PauseDetection) THEN
				//Leakage calculation and detection
				FOR i:=0 TO 2 DO 
					
					IF (ReferenceFilter.y - LeakageFilters[i].y) > LeakageThresholds[i] THEN
						LeakageAccumulator[i] := LeakageAccumulator[i] +  ((ReferenceFilter.y - LeakageFilters[i].y) - LeakageThresholds[i])*Dt;
						IF LeakageAccumulator[i] > LeakageTripLevels[i] THEN
							LeakageLevel[i] := TRUE;
						END_IF
					END_IF
					IF (ReferenceFilter.y - LeakageFilters[i].y) < LeakageThresholds[i] AND LeakageAccumulator[i] > 0.0 THEN
						LeakageAccumulator[i] := LeakageAccumulator[i] +  ((ReferenceFilter.y - LeakageFilters[i].y) - LeakageThresholds[i])*Dt;
						IF LeakageAccumulator[i] > LeakageTripLevels[i] THEN
							LeakageLevel[i] := TRUE;
						END_IF
						IF LeakageAccumulator[i] < 0.0 THEN
							LeakageLevel[i] := FALSE;
						END_IF
					END_IF	
					
				END_FOR
			END_IF
			
			TON_LeakageAlarmTimer[0].IN := LeakageLevel[0];
					
			IF TON_LeakageAlarmTimer[0].Q THEN
				LeakageAccumulator[0] := 0;
				LeakageLevel[0] := FALSE;
				LeakageFilters[0](Set_y := 1, y_set :=  ReferenceFilter.y, u := ReferenceFilter.y);
				LeakageFilters[0](Set_y := 0);
			END_IF
				
		END_IF
		
	ELSE
		
		// If block is disabled, return 0s and do not evaluate any filters. Deinitialize filters

		
		FOR i:=0 TO 2 DO 
			
			LeakageLevel[i] := FALSE;
			LeakageAccumulator[i] := 0.0;
			TON_LeakageAlarmTimer[i].IN  := FALSE;	
		END_FOR
		
		AccumulatedInput :=0.0;
		InitSamples := 0.0;
		AvgInitValue := 0.0;
		Initialized := FALSE;
		TON_EvaluationTimer.IN := FALSE;
		
	END_IF
	
	
	TON_EvaluationTimer();
	ReferenceFilter( u := SystemOilMass);
	FOR i:=0 TO 2 DO 
		LeakageFilters[i]( u := SystemOilMass);
		TON_LeakageAlarmTimer[i]();
	END_FOR
	
END_FUNCTION_BLOCK
