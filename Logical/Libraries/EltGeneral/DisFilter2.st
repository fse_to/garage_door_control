(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: EltGeneral
 * File: DisFilter2.st
 * Author: villefrancea
 * Created: November 27, 2013
 *******************************************************************)

FUNCTION_BLOCK DisFilter2
	IF init THEN
		init := FALSE;
		RTInfo_0(enable:=TRUE);
		dt := UDINT_TO_REAL(RTInfo_0.cycle_time) * 0.000001;
		IF RunAtHalfFrequency THEN
			dt := dt * 2;
		END_IF
	END_IF
	debug := 0;
	
	IF initFilter THEN
		initFilter := FALSE;
		x1Last := initVelo;
		x2Last := initPos;
	END_IF
	IF (uIn > MaxLimit) THEN
		uIn_limited := MaxLimit;
		debug.0 := TRUE;
	ELSIF(uIn < MinLimit) THEN
		uIn_limited := MinLimit;
		debug.1 := TRUE;
	ELSE
		uIn_limited := uIn;
	END_IF
	
	//uIn := LIMIT(MinLimit, uIn, MaxLimit);
	
	
	x1Velocity := Ad11 * x1Last + Ad12 * x2Last + B1 * uIn_limited;
	x2Position := Ad21 * x1Last + Ad22 * x2Last + B2 * uIn_limited;
	
	
	//Acc Limit
	IF x1Velocity - x1Last > accMax*dt THEN
		debug.2 := TRUE;
		x1Velocity := x1Last + (accMax*dt);
		x2Position := x2Last + (x1Velocity*dt);
	ELSIF x1Velocity - x1Last < -accMax*dt THEN
		debug.3 := TRUE;
		x1Velocity := x1Last - (accMax*dt);
		x2Position := x2Last + (x1Velocity*dt);
	END_IF

	
	//Recalculation of speed if maximum speed is exceeded
	IF x1Velocity < -x1VelocityMax THEN
		debug.4 := TRUE;
		x1Velocity := -x1VelocityMax;
		x2Position := x2Last + (x1Velocity * dt);
	ELSIF x1Velocity > x1VelocityMax THEN
		debug.5 := TRUE;
		x1Velocity := x1VelocityMax;
		x2Position := x2Last + (x1Velocity * dt);
	END_IF

	MaxVel_ToLimit := SQRT(ABS( MaxLimit-x2Position) *accMax );
	MinVel_ToLimit := -SQRT(ABS(x2Position - MinLimit ) *accMax );
	IF x1Velocity > MaxVel_ToLimit THEN
		x1Velocity := MaxVel_ToLimit;
		x2Position := x2Last + (x1Velocity *dt);
		debug.6 := 1;
	ELSIF x1Velocity < MinVel_ToLimit THEN
		x1Velocity := MinVel_ToLimit;
		x2Position := x2Last + (x1Velocity *dt);
		debug.7 := 1;
	END_IF
	
	x1Last := x1Velocity;
	x2Last := x2Position;
	

END_FUNCTION_BLOCK

