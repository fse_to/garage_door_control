(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Library: EltGeneral
 * File: AngleCylPos.st
 * Author: avi
 * Created: October 07, 2011
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

FUNCTION_BLOCK AngleToCyl
	phiRad := (anglePos +const_Phi0 + caliAngleOffset)*CONST_PI/180;
	Lcyl := SQRT(const_Xc**2 + const_R**2 - 2*const_Xc*const_R*COS(phiRad));	
	cylPos := Lcyl - calib_LcylMin;

	cosAngleOfAttack := (Lcyl**2 + const_R**2 - const_Xc**2) / (2*Lcyl*const_R);
	torqueArm := SIN(ACOS(cosAngleOfAttack))*const_R;

	ratio := Lcyl / (const_R*const_Xc* SQRT(1-( (const_R**2 + const_Xc**2 - Lcyl**2) /(2*const_R*const_Xc))**2));
	cylVelo := angleVelo*CONST_PI/180/ratio;	
END_FUNCTION_BLOCK 

FUNCTION_BLOCK CylToAngle
	Lcyl 	:= cylPos + calib_LcylMin;
	cosPhi 	:= (const_R**2 + const_Xc**2 - Lcyl**2)/(2*const_R*const_Xc);
	phiRad := ACOS(cosPhi);
	anglePos := (phiRad * 180/CONST_PI) - const_Phi0 -caliAngleOffset;

	cosAngleOfAttack := (Lcyl**2 + const_R**2 - const_Xc**2) / (2*Lcyl*const_R);
	torqueArm := SIN(ACOS(cosAngleOfAttack))*const_R;

	ratio := Lcyl / (const_R*const_Xc* SQRT(1-( (const_R**2 + const_Xc**2 - Lcyl**2) /(2*const_R*const_Xc))**2));	
	
	angleVelo := cylVelo* ratio *180/CONST_PI;
END_FUNCTION_BLOCK 

