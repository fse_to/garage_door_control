(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: EltGeneral
 * File: LevelSteps.st
 * Author: villefrancea
 * Created: June 22, 2016
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

(* TODO: Add your comment here *)
FUNCTION_BLOCK LevelStepsWithDelay
	
	(*Idea is that is is level escalating.*)
	indexMax := LIMIT(0, indexMax, 3);
	level := MIN(level, indexMax+1);
	levelOld := level;
	i:= indexMax;
	FOR j:=0 TO indexMax DO		
		IF InIncreasingValue=FALSE THEN
			IF level <=i+1 AND in < levelEntry[i] THEN
				level := i+1;
			ELSIF level=i+1 AND in > levelExit[i] THEN
				level := i;
			END_IF
		ELSE
			IF level <=i+1 AND in > levelEntry[i] THEN
				level := i+1;
			ELSIF level=i+1 AND in < levelExit[i] THEN
				level := i;
			END_IF			
		END_IF
		i := i - 1;
	END_FOR
	IF level >= levelOld  AND TOF_SWITCHDELAY.Q = FALSE THEN
		level := levelOld+1;
		TOF_SWITCHDELAY.IN:=TRUE;
	ELSE
		TOF_SWITCHDELAY.IN :=FALSE;
		level := levelOld;
	END_IF
	TOF_SWITCHDELAY(PT:=WaitTime);
	
END_FUNCTION_BLOCK
