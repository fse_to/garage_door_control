(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: EltGeneral
 * File: RampVelo.st
 * Author: villefrancea
 * Created: November 27, 2013
 *******************************************************************)

FUNCTION_BLOCK RampVelo
	IF init THEN
		init := FALSE;
		RTInfo_0(enable:=TRUE);
		dt := UDINT_TO_REAL(RTInfo_0.cycle_time) * 0.000001;
	END_IF
	
	IF initFilter THEN
		initFilter := FALSE;
		x1Velocity := initVelo;
		x2Position := initPos;
	END_IF
	
	ang_limit := 0.5*x1VelocityMax*x1VelocityMax/accMax;
	
	IF reference > x2Position THEN
		IF (reference - x2Position)<ang_limit THEN
			x1Velocity := x1Velocity - accMax*dt;
			x1Velocity := LIMIT(1, x1Velocity, x1VelocityMax);
		ELSE
			x1Velocity := x1Velocity + accMax*dt;
			//x1Velocity := LIMIT(-x1VelocityMax, x1Velocity, x1VelocityMax);
		END_IF
	ELSIF reference < x2Position THEN
		IF (x2Position - reference)<ang_limit THEN
			x1Velocity := x1Velocity + accMax*dt;
			x1Velocity := LIMIT(-x1VelocityMax, x1Velocity, -1);
		ELSE
			
			x1Velocity := x1Velocity - accMax*dt;
		END_IF
		
	END_IF
		x1Velocity := LIMIT(-x1VelocityMax, x1Velocity, x1VelocityMax);
	IF (x1Velocity*x1Last) < 0 (*OR (initPos-reference)*(x2Position-reference) <0*) THEN
//		x1Velocity := 0;
//		x2Position := reference;
	END_IF
	
	x2Position := x2Position + x1Velocity * dt;

	
	debug := 0;
	x1Last := x1Velocity;
	x2Last := x2Position;
END_FUNCTION_BLOCK

