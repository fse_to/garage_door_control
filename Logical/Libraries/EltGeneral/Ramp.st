(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: EltGeneral
 * File: ramp.st
 * Author: villefrancea
 * Created: November 28, 2013
 *******************************************************************)

FUNCTION_BLOCK Ramp
	IF init THEN
		init := FALSE;
		RTInfo_0(enable:=TRUE);
		dt := UDINT_TO_REAL(RTInfo_0.cycle_time) * 0.000001;
		rampingActive := FALSE;
	END_IF
	
	IF initRamp THEN
		initRamp := FALSE;
		lastIn := initPos;
		rampingActive := FALSE;
	END_IF
	
	
	IF ABS(lastIn - in) > RampStartDiff THEN
		rampingActive := TRUE;
		rampingDirPos :=  in > lastIn;
	END_IF
	IF rampingActive THEN
		IF ABS(out - in) < 4.0 THEN
			factor := 0.5;
		ELSE
			factor := 1;
		END_IF
		IF rampingDirPos THEN
			out := out + (VelocityMax * dt * factor);
		ELSE
			out := out - (VelocityMax * dt * factor);
		END_IF
			
		IF ABS(out - in) < RampEndDiff THEN
			rampingActive := FALSE;
		END_IF
		
	ELSE
		out := in;
	END_IF

	lastIn := in;
	
END_FUNCTION_BLOCK
				   