
(* TODO: Add your comment here *)
FUNCTION_BLOCK createTimeString
	
	strcpy(ADR(timeString),ADR(''));
	
	DTGetTime_1.enable:=1;
	DTGetTime_1();
	DT1:=DTGetTime_1.DT1; 
	DT_TO_DTStructure(DT1,ADR(DT1_structure));

	itoa(DT1_structure.year,ADR(MC_year_string));
	itoa(DT1_structure.month,ADR(MC_month_string));
	itoa(DT1_structure.day,ADR(MC_day_string));
	itoa(DT1_structure.hour,ADR(MC_hour_string));
	itoa(DT1_structure.minute,ADR(MC_min_string));
	itoa(DT1_structure.second,ADR(MC_sec_string));
				
	strcpy(ADR(timeString),ADR(MC_year_string));
	strcat(ADR(timeString),ADR('/'));
	strcat(ADR(timeString),ADR(MC_month_string));
	strcat(ADR(timeString),ADR('/'));
	strcat(ADR(timeString),ADR(MC_day_string));
	strcat(ADR(timeString),ADR(' '));
	strcat(ADR(timeString),ADR(MC_hour_string));
	strcat(ADR(timeString),ADR(':'));
	strcat(ADR(timeString),ADR(MC_min_string));
	strcat(ADR(timeString),ADR(':'));
	strcat(ADR(timeString),ADR(MC_sec_string));
	
END_FUNCTION_BLOCK
