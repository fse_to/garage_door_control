
(* TODO: implement detailed control of PWM control *)
FUNCTION_BLOCK PWM_control
	
	// 0.1 A = 0 bar, 0.6 A = 450 bar. Approximately. 
	// pulseWidth 0->32767 is 0-100 %
	// 100 % is ~1 A (cold coil). 
	
//	pulseWidth := LIMIT(REAL_TO_INT(66.3*setpoint+2945),0,32767); 
	
	IF enableCL THEN
		CLgain := LIMIT(0.8,setpoint/measured,1.2); 
	ELSE 
		CLgain := 1;
	END_IF
	
//	pulseWidth := LIMIT(REAL_TO_INT(56.2*setpoint*CLgain+1635),0,32767);
	
	pulseWidth := LIMIT(REAL_TO_INT(40*setpoint*CLgain+1635),0,32767); 
	
END_FUNCTION_BLOCK
