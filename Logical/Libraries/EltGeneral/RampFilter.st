(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: EltGeneral
 * File: ramp.st
 * Author: villefrancea
 * Created: November 28, 2013
 *******************************************************************)

FUNCTION_BLOCK RampFilter
	IF init THEN
		DisFilter2_0(init:=TRUE);
		RampVelo_0(init:=TRUE);
		init := FALSE;
		rampingActive := FALSE;
		
		// 100 ms filter 
		
		DisFilter2_0.Ad11 := 0.8144;
		DisFilter2_0.Ad12 := -0.9048;
		DisFilter2_0.Ad21 := 0.0090; 
		DisFilter2_0.Ad22 := 0.9953;
		DisFilter2_0.B1 := 0.9048; 
		DisFilter2_0.B2 := 0.0047;
	
		
		// 50 ms filter
		(*
		DisFilter2_0.Ad11 := 0.6550;
		DisFilter2_0.Ad12 := -3.2749;
		DisFilter2_0.Ad21 := 0.0082; 
		DisFilter2_0.Ad22 := 0.9825;
		DisFilter2_0.B1 := 3.2749; 
		DisFilter2_0.B2 := 0.0175;
			*)
		// 10 ms filter
		(*
		DisFilter2_0.Ad11 := 0.0;
		DisFilter2_0.Ad12 := -36.7879;
		DisFilter2_0.Ad21 := 0.0037; 
		DisFilter2_0.Ad22 := 0.7358;
		DisFilter2_0.B1 := 36.7879; 
		DisFilter2_0.B2 := 0.2642;
*)
	END_IF
	
	DisFilter2_0.accMax := accMax;
	DisFilter2_0.x1VelocityMax := VelocityMax; 
	RampVelo_0.accMax := accMax;
	RampVelo_0.x1VelocityMax := VelocityMax;
	
	IF initRamp THEN
		initRamp := FALSE;
		lastRef := reference;
		rampingActive := TRUE;
		firstRun := TRUE;
		x2Position := initPos;
	END_IF
	
	
	IF ABS(lastRef - reference) > RampStartDiff THEN
	//IF ABS(lastRef - reference) > RampStartDiff OR ABS(reference - gMCtrl.io.blade[0].PITCHANGLEA.vPos)>4 THEN
		rampingActive := TRUE;
	END_IF
	firstRun  := EDGE(rampingActive);
	
	// rampingActive := FALSE; // Forcing ramping off, should be removed later!!!!!!!!!!!!!!!!!!!!!!
	// firstRun = FALSE; // Forcing initialization off, should be removed later!!!!!!!!!!
	
	IF rampingActive THEN
		IF firstRun THEN
			RampVelo_0.initFilter := TRUE;
			RampVelo_0.initVelo := x1Velocity;
			RampVelo_0.initPos := x2Position;
			firstRun := FALSE;
		END_IF
		
		RampVelo_0.reference := reference;
		RampVelo_0();
	//	DisFilter2_0();
		
		x1Velocity := RampVelo_0.x1Velocity;
		x2Position := RampVelo_0.x2Position;
		
		IF ABS(x2Position - reference) < RampEndDiff THEN
			rampingActive := FALSE;
		END_IF
		
	ELSE
		IF firstRun THEN
			DisFilter2_0.initFilter := TRUE;
			DisFilter2_0.initVelo := x1Velocity;
			DisFilter2_0.initPos := x2Position;
			firstRun := FALSE;
		END_IF 
		
		DisFilter2_0.uIn := reference;
		RampVelo_0.reference := reference;
		RampVelo_0();
		DisFilter2_0();
		x1Velocity := DisFilter2_0.x1Velocity;
		x2Position := DisFilter2_0.x2Position;
	END_IF

	lastRef := reference;
	debug := DisFilter2_0.debug;
	
END_FUNCTION_BLOCK
				   