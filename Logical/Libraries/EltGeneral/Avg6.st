(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: EltGeneral
 * File: Avg6.st
 * Author: villefrancea
 * Created: May 25, 2016
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

(* TODO: Add your comment here *)
FUNCTION_BLOCK Avg6
	enableCount := 0;
	averageSum := 0;
	FOR i:=0 TO MIN(lastIndex, 5) DO
		IF enableCount>=1 AND enable[i] THEN
			enableCount := enableCount +1;
			minimum := MIN( minimum, in[i]);
			maximum := MAX( maximum, in[i]);
			averageSum := averageSum + in[i];
		ELSIF enable[i] THEN
			enableCount := 1;
			minimum := in[i];
			maximum := in[i];
			averageSum := in[i];
		END_IF
	END_FOR
	IF enableCount=0 THEN (*No valid values*)
		minimum := 0;
		maximum := 0;
		average := 0;
	ELSE
		average := averageSum / USINT_TO_REAL(enableCount);
	END_IF	
END_FUNCTION_BLOCK
