(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: EltGeneral
 * File: DisFilter2.st
 * Author: villefrancea
 * Created: November 27, 2013
 *******************************************************************)

FUNCTION_BLOCK DisDelay
	IF delay > 10 THEN
		delay := 10;
	END_IF
	IF delay = 0 OR init THEN
		init := FALSE;
		buffer[0] := in;
		buffer[1] := in;
		buffer[2] := in;
		buffer[3] := in;
		buffer[4] := in;
		buffer[5] := in;
		buffer[6] := in;
		buffer[7] := in;
		buffer[8] := in;
		buffer[9] := in;		
		out := in;		
	ELSIF delay>=1 AND delay<=10 THEN
		index := delay - 1;
		out := buffer[0];
		//Overwrite above;
		buffer[0] := buffer[1];
		buffer[1] := buffer[2];
		buffer[2] := buffer[3];
		buffer[3] := buffer[4];
		buffer[4] := buffer[5];
		buffer[5] := buffer[6];
		buffer[6] := buffer[7];
		buffer[7] := buffer[8];
		buffer[8] := buffer[9];
		buffer[9] := in;
		buffer[index] := in;
	END_IF
	
END_FUNCTION_BLOCK