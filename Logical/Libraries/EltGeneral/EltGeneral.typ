(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Library: EltGeneral
 * File: EltGeneral.typ
 * Author: avi
 * Created: September 21, 2011
 ********************************************************************
 * Data types of library EltGeneral
 ********************************************************************)

TYPE
	AlrCtrlProc_enum : 
		(
		ALR_OK := 0,
		ALR_WARNING := 10,
		ALR_LVRT := 25,
		ALR_STOP := 50,
		ALR_UCSTOP := 200,
		ALR_SAFETY := 210
		);
	propvalve_typ : 	STRUCT 
		fb : INT;
		v : INT;
	END_STRUCT;
	do_typ : 	STRUCT 
		status : BOOL;
		v : BOOL;
	END_STRUCT;
	sdo_typ : 	STRUCT 
		v : BOOL;
		currentOk : BOOL;
	END_STRUCT;
	AlrResetTypes_enum : 
		(
		ALRAUTORESET := 10,
		ALRMANRESET := 20,
		ALRLOCALRESET := 30,
		ALRSAFERESET := 50,
		ALRNORESET := 40
		);
END_TYPE
