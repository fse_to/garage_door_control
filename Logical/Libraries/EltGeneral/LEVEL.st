(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Library: EltGeneral
 * File: LEVEL.st
 * Author: avi
 * Created: September 21, 2011
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

(* Calculated AI input to Level in percentage. *)
FUNCTION_BLOCK LEVEL
	// Level sensor. Input signal in the range 4...20mA -> 0...32767 -> 0.... 100%
	IF hw>32500 THEN
		v := 999.99;
		status := 1;
	ELSIF hw<-1000 THEN
		v := -9.99;
		status := 2; 
	ELSIF hw<1000 THEN
		v := 0;
		status := 3;
	ELSE
		v := slope*hw+min_vol;
		//GW3 and 0Series
		//v := (hw*0.020654)+135.81;
		//v := (hw*0.017668)+265.85;
		//v := (hw*0.017668)+176.811; // Value in [L]. See Oil level sensor calibration sheet
		//v := (hw*0.024682)+86.3;
		
		status := ERR_OK;
	END_IF
END_FUNCTION_BLOCK