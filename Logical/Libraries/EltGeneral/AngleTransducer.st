FUNCTION_BLOCK AngleTransducer
	
	ACONST := 15.59;
	BCONST := 119.99;
	DCONST := 77.5;
	
	l := (INT_TO_REAL(bitSignal)/32767.0 *5.0) + 5.0;
	c := BCONST - l;
	e := SQRT( (c*c)+(ACONST*ACONST) );
	beta := ACOS( ( (c*c) + (e*e) - (ACONST*ACONST) ) / (2*c*e) );
	gamma := ACOS(DCONST/e);
	alpha := gamma-beta;
	alphaDeg := alpha*180/CONST_PI;
	
	
END_FUNCTION_BLOCK
