(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: EltGeneral
 * File: MotorLevel.st
 * Author: villefrancea
 * Created: June 21, 2016
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

(* TODO: Add your comment here *)
FUNCTION_BLOCK MotorLevel
	(*Search for index with lowest onTime and OffTime*)
	HighestOffTime := 0;
	HighestOnTime := 0;
	HighestOnIndex := 99;
	HighestOffIndex := 99;
	actualLevel := 0;
	FOR i:=0 TO 3 DO
		IF enable[i] THEN
			IF on[i] THEN
				actualLevel := actualLevel + 1;
				IF OnTime[i] >= HighestOnTime THEN
					HighestOnTime := OnTime[i];
					HighestOnIndex := i;
				END_IF
			ELSE	
				IF OffTime[i] >= HighestOffTime THEN
					HighestOffTime := OffTime[i];
					HighestOffIndex := i;
				END_IF
			END_IF	
		ELSE
			on[i] := FALSE;
		END_IF
	END_FOR
	(*Check to see if enough is activated according to level*)
	IF actualLevel < level AND HighestOffIndex <=3 THEN
		on[HighestOffIndex] := TRUE;
	ELSIF actualLevel > level AND HighestOnIndex <=3 THEN
		on[HighestOnIndex] := FALSE;
	END_IF
	
			
	
	TON_1s.PT:=T#1s;
	TON_1s.IN := TRUE;
	
	IF TON_1s.Q THEN
		TON_1s.IN := FALSE;
		FOR i:=0 TO 3 DO		
			IF on[i] THEN
				OffTime[i] := 0;
				OnTime[i] := OnTime[i] + 1;
			ELSE
				OnTime[i] := 0;
				OffTime[i] := OffTime[i] + 1;
			END_IF
		END_FOR
	END_IF
	TON_1s();
				
	
	
END_FUNCTION_BLOCK
