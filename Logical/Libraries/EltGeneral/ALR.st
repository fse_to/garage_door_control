(********************************************************************
 * COPYRIGHT -- Eltronic
 ********************************************************************
 * Library: ELT_StdLib
 * File: ALR.st
 * Author: avi
 * Created: November 12, 2010
 ********************************************************************
 * Implementation of library ELT_StdLib
 ********************************************************************) 

(* Alarm FUB *)
// -----------------------------------------------------------------------------------------------------
FUNCTION_BLOCK ALR
	name;
	motionCtrl;
	PWUCtrl;
	OfflineCtrl;
	number;
	
	//Call parameters in FB	
	oldState := state;
	//Reset conditions AutoReset		
	IF state = TRUE AND setReset=FALSE AND resetType = ALRAUTORESET THEN
		state := FALSE;
		TON_AlarmTimer(IN:=FALSE);
	END_IF
	// Manual reset
	IF resetLatch=TRUE AND ( resetType = ALRMANRESET OR resetType = ALRAUTORESET ) THEN
		state := FALSE;
		TON_AlarmTimer(IN:=FALSE);
	END_IF
	// Local reset
	IF resetLocalLatch=TRUE AND ( resetType = ALRLOCALRESET ) THEN
		state := FALSE;
		TON_AlarmTimer(IN:=FALSE);
	END_IF
	// Safe reset
	IF resetSafeLatch=TRUE AND ( resetType = ALRSAFERESET ) THEN
		state := FALSE;
		TON_AlarmTimer(IN:=FALSE);
	END_IF
	//Set conditions
	IF state = FALSE THEN
		TON_AlarmTimer(IN:=setReset, PT:= setDelay);
		IF TON_AlarmTimer.Q THEN
			TON_AlarmTimer(IN:=FALSE);
			state := TRUE;
		END_IF
	END_IF
	
	//Reset alarms if the set/reset is not set. Dette if-statement synes unødvendigt ,jeb. 
	IF setReset = FALSE AND resetLatch AND (resetType = ALRMANRESET OR resetType = ALRAUTORESET ) THEN // Only reset alarms that is resetable. Some alarms should only be reset by shutting off the UPS-power
		state := FALSE;
	END_IF
	//Reset state if the alarm is not enabled.
	IF disable = TRUE THEN
		state := FALSE;
	END_IF
	//Write to log
	IF oldState <> state THEN
		AlarmLog(number, name, state);
	END_IF			
END_FUNCTION_BLOCK
// -----------------------------------------------------------------------------------------------------
