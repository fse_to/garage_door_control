(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Library: EltGeneral
 * File: DiffInt.st
 * Author: avi
 * Created: November 03, 2011
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

(* Calculates the difference between to integer values and return abs value. *)
FUNCTION DiffInt
	DiffInt := ABS(IN1 - IN2);
END_FUNCTION

(* Calculates the difference between to real values and return abs value. *)
FUNCTION DiffReal
	DiffReal := ABS(IN1 - IN2);
END_FUNCTION