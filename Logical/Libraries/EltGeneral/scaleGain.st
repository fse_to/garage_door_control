(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Program: MotionCtrl
 * File: scaleGain.st
 * Author: avi
 * Created: April 24, 2013
 *******************************************************************)

FUNCTION scaleGain
	
	IF x1 = x2 THEN
		scaleGain := 0;		
	ELSE
			
		scaleGain := (y2-y1)/(x2-x1)*(x-x1)+y1; //y1 - ((y1-y2)*x/x2);
		
		IF scaleGain>MAX(y1,y2) THEN
			
			scaleGain:= MAX(y1,y2);
			
		ELSIF scaleGain<MIN(y1,y2) THEN
			
			scaleGain := MIN(y1,y2);
			
		END_IF
		
	END_IF
	

END_FUNCTION
