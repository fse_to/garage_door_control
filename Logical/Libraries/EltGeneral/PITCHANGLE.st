(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Library: EltGeneral
 * File: PITCHANGLE.st
 * Author: avi
 * Created: September 21, 2011
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 

(* Calculated Pitch Angle and Speed from a DC2190 module *)
FUNCTION_BLOCK PITCHANGLE
	hwUS_Speed;
	EvalRodError; 
	cylPos := DINT_TO_REAL(hwPos)*0.001;
	cylSpeed :=INT_TO_REAL(hwSpeed)*0.1;
	
	CylToAngle_0(cylPos :=cylPos, cylVelo :=cylSpeed , caliAngleOffset:= caliAngleOffset, calib_LcylMin := calib_LcylMin,
		const_Phi0 := const_Phi0, const_R := const_R, const_Xc := const_Xc );
	
	// Make sure there is always and old-value
	IF NOT(EvalRodError) THEN
		oldCylPos := cylPos; 
		oldCylSpeed := cylSpeed; 
	END_IF
	

	CASE status OF
		0:
			
			IF hwRodErr THEN 
				status:= 1;
			ELSIF (ABS(oldCylPos - cylPos) >= 2 OR ABS(oldCylSpeed- cylSpeed) >= 13) AND EvalRodError  THEN
				status := 2;	
			ELSE // Everything is ok
				oldCylPos:= cylPos;
				oldCylSpeed := cylSpeed;
				
				vPos := CylToAngle_0.anglePos;
				vSpeed := CylToAngle_0.angleVelo;
				strokeDisplay := hwPos; 
				angleDisplay := vPos;
				torqueArm := CylToAngle_0.torqueArm;
				maxStrokeOut := maxStrokeIn;
			END_IF
			

			
		1:
			vSpeed := 0;	
			strokeDisplay := -1000000000; 
			angleDisplay := -100;
			
			IF NOT(hwRodErr) THEN
				status := 0;
				oldCylPos:= cylPos;
				oldCylSpeed := cylSpeed;
			END_IF
		
		2:	
			vSpeed := 0;	
			strokeDisplay := -1000000000; 
			angleDisplay := -100;
			
			TON_cylinderFreeze.IN :=ABS(oldCylPos - cylPos) <= 20 AND cylPos > 0.0;
			TON_cylinderFreeze.PT := T#1s;
			TON_cylinderFreeze();
			
			IF hwRodErr THEN 
				status := 1;
				TON_cylinderFreeze(IN :=FALSE);
			ELSIF TON_cylinderFreeze.Q THEN
				status := 0;
				oldCylPos:= cylPos;
				oldCylSpeed := cylSpeed;
				TON_cylinderFreeze(IN :=FALSE);
			ELSE
				oldCylPos:= cylPos;
				oldCylSpeed := cylSpeed;
			END_IF
		
			

	END_CASE;
	


	
END_FUNCTION_BLOCK