(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Library: EltGeneral
 * File: NewFunctionBlock.st
 * Author: avi
 * Created: May 20, 2012
 ********************************************************************
 * Implementation of library EltGeneral
 ********************************************************************) 
FUNCTION_BLOCK SelectorWithDelay


	CASE STATE OF
		0: //Both outputs off
			Q1 := FALSE;
			Q2 := FALSE;
			TON_SWITCHDELAY(IN:=TRUE, PT := PT);
			IF TON_SWITCHDELAY.Q THEN
				IF IN = 1  THEN
					STATE := 1;
					TON_SWITCHDELAY(IN:= FALSE);
				ELSIF IN = 2 THEN
					STATE := 2;
					TON_SWITCHDELAY(IN:= FALSE);
				END_IF
			END_IF
	
		1:
			Q1 := TRUE;
			Q2 := FALSE;
			IF IN <> 1 THEN
				STATE := 0;
			END_IF
	
		2: 
			Q1 := FALSE;
			Q2 := TRUE;
			IF IN <> 2 THEN
				STATE := 0;
			END_IF

	END_CASE

END_FUNCTION_BLOCK