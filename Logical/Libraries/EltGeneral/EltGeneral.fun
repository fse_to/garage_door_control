(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Library: EltGeneral
 * File: EltGeneral.fun
 * Author: avi
 * Created: September 21, 2011
 ********************************************************************
 * Functions and function blocks of library EltGeneral
 ********************************************************************)

FUNCTION_BLOCK IdentifySystem
	VAR_INPUT
		SerialNumber : UDINT;
	END_VAR
	VAR_OUTPUT
		GW0_series : BOOL;
		TEST_RIG_PITCH : BOOL;
		TEST_RIG_LOAD : BOOL;
		GW1 : BOOL;
		GW2 : BOOL;
		GW3 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK AngleTransducer
	VAR_INPUT
		bitSignal : INT;
	END_VAR
	VAR_OUTPUT
		alphaDeg : REAL;
	END_VAR
	VAR
		alpha : REAL;
		gamma : REAL;
		beta : REAL;
		e : REAL;
		l : REAL;
		c : REAL;
		ACONST : REAL;
		BCONST : REAL;
		DCONST : REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK SCALE (*Converts AI pressure inputs to Real value in Bar*)
	VAR_INPUT
		hw : INT;
		scale4mA : REAL;
		scale20mA : REAL;
	END_VAR
	VAR_OUTPUT
		v : REAL;
		status : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TEMP (*Converts AI pressure inputs to Real value in Bar*)
	VAR_INPUT
		hw : INT;
		scale4mA : REAL;
		scale20mA : REAL;
	END_VAR
	VAR_OUTPUT
		v : REAL;
		status : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK PRESSURE (*Converts AI pressure inputs to Real value in Bar*)
	VAR_INPUT
		hw_offset : INT;
		hw : INT;
		sensorBar : UINT;
	END_VAR
	VAR_OUTPUT
		v : REAL;
		status : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK PT100 (*Converts PT100 inputs to Real value in �C*)
	VAR_INPUT
		hw : INT;
	END_VAR
	VAR_OUTPUT
		v : REAL;
		status : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK PITCHANGLE (*Calculated Pitch Angle and Speed from a DC2190 module*)
	VAR_INPUT
		EvalRodError : BOOL;
		hwPos : DINT;
		hwSpeed : INT;
		caliAngleOffset : REAL;
		hwRodErr : BOOL;
		hwUS_Speed : UDINT;
		calib_LcylMin : REAL;
		const_Xc : REAL;
		const_R : REAL;
		maxStrokeIn : REAL;
		const_Phi0 : REAL;
	END_VAR
	VAR_OUTPUT
		maxStrokeOut : REAL;
		vPos : REAL;
		vSpeed : REAL;
		torqueArm : REAL;
		angleDisplay : REAL;
		strokeDisplay : REAL;
		status : UINT;
	END_VAR
	VAR
		CylToAngle_0 : CylToAngle;
		oldCylSpeed : REAL;
		cylSpeed : REAL;
		oldCylPos : REAL;
		cylPos : REAL;
		TON_cylinderFreeze : TON;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK LEVEL (*Calculated AI input to Level in percentage.*)
	VAR_INPUT
		slope : REAL;
		min_vol : REAL;
		hw : INT;
	END_VAR
	VAR_OUTPUT
		v : REAL;
		status : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK ALR (*Alarmblock*)
	VAR_INPUT
		number : USINT := 255;
		name : STRING[40] := 'not set name';
		motionCtrl : AlrCtrlProc_enum := ALR_OK;
		PWUCtrl : AlrCtrlProc_enum := ALR_OK;
		OfflineCtrl : AlrCtrlProc_enum := ALR_OK;
		resetType : AlrResetTypes_enum := ALRAUTORESET;
		setReset : BOOL;
		setDelay : TIME := T#0S;
		resetLatch : BOOL;
		resetSafeLatch : BOOL;
		resetLocalLatch : BOOL;
		disable : BOOL;
	END_VAR
	VAR_OUTPUT
		state : BOOL;
	END_VAR
	VAR
		TON_AlarmTimer : TON;
		oldState : BOOL;
	END_VAR
END_FUNCTION_BLOCK
(*Log functions*)

FUNCTION UserLog : USINT
	VAR_INPUT
		username : STRING[40];
		logAction : BOOL;
		sessionDuration : UINT;
	END_VAR
	VAR
		Args : BRSE_ARL_Arguments_typ;
	END_VAR
END_FUNCTION

FUNCTION AlarmLog : BOOL (*Writes an alarm text to XML*)
	VAR_INPUT
		number : USINT; (*Alarm number*)
		nameText : STRING[40]; (*Name of alarm*)
		newState : BOOL; (*state of alarm*)
	END_VAR
	VAR
		Args : BRSE_ARL_Arguments_typ;
		tempType : STRING[16];
	END_VAR
END_FUNCTION

FUNCTION EventLog : BOOL (*Writes an event text to XML*)
	VAR_INPUT
		nameText : STRING[40]; (*Name of event text*)
		descText : STRING[128]; (*Description of the event*)
		groupText : STRING[16]; (*Name of the group/object the event belongs to*)
		number : UINT; (*Not used*)
		value1 : REAL; (*Event state number*)
		value2 : REAL; (*Not used*)
		unit : STRING[8]; (*Not used*)
	END_VAR
	VAR
		Args : BRSE_ARL_Arguments_typ;
		tempType : STRING[16];
	END_VAR
END_FUNCTION

FUNCTION DebugLog : BOOL (*Debug Text Log*)
	VAR_INPUT
		nameText : STRING[40]; (*Name of debug text*)
		descText : STRING[128]; (*Description of the debug*)
		groupText : STRING[16]; (*Name of the group/object the debug belongs to*)
		number : UINT; (*not used*)
		value1 : REAL; (*previously value*)
		value2 : REAL; (*new value*)
		unit : STRING[8]; (*unit of parameter*)
	END_VAR
	VAR
		Args : BRSE_ARL_Arguments_typ;
		tempType : STRING[16];
	END_VAR
END_FUNCTION

FUNCTION_BLOCK CylToAngle
	VAR_INPUT
		cylPos : REAL;
		cylVelo : REAL;
		caliAngleOffset : REAL;
		calib_LcylMin : REAL;
		const_Xc : REAL;
		const_R : REAL;
		const_Phi0 : REAL;
	END_VAR
	VAR_OUTPUT
		anglePos : REAL;
		angleVelo : REAL;
		torqueArm : REAL;
	END_VAR
	VAR
		Lcyl : REAL;
		cosPhi : REAL;
		phiRad : REAL;
		cosAngleOfAttack : REAL;
		ratio : REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK AngleToCyl
	VAR_INPUT
		anglePos : REAL;
		angleVelo : REAL;
		caliAngleOffset : REAL;
		calib_LcylMin : REAL;
		const_Xc : REAL;
		const_R : REAL;
		const_Phi0 : REAL;
	END_VAR
	VAR_OUTPUT
		cylPos : REAL;
		cylVelo : REAL;
		torqueArm : REAL;
	END_VAR
	VAR
		phiRad : REAL;
		Lcyl : REAL;
		cosAngleOfAttack : REAL;
		ratio : REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION DiffInt : UINT (*Calculates the difference between to interger values and return abs value.*)
	VAR_INPUT
		IN1 : INT;
		IN2 : INT;
	END_VAR
END_FUNCTION

FUNCTION DiffReal : REAL (*Calculates the difference between to interger values and return abs value.*)
	VAR_INPUT
		IN1 : REAL;
		IN2 : REAL;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK SelectorWithDelay (*Select between two outputs and use a delay between selection.*)
	VAR_INPUT
		IN : USINT;
		PT : TIME;
	END_VAR
	VAR_OUTPUT
		Q1 : BOOL;
		Q2 : BOOL;
	END_VAR
	VAR
		STATE : USINT;
		TON_SWITCHDELAY : TON;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK DisDelay
	VAR_INPUT
		in : REAL;
		delay : USINT;
		init : BOOL;
	END_VAR
	VAR_OUTPUT
		out : REAL;
	END_VAR
	VAR
		buffer : ARRAY[0..9] OF REAL;
		index : USINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK RampFilter
	VAR_INPUT
		VelocityMax : REAL;
		accMax : REAL;
		reference : REAL;
		init : BOOL;
		initRamp : BOOL;
		initPos : REAL;
		RampStartDiff : REAL := 4.5;
		RampEndDiff : REAL := 0.05;
	END_VAR
	VAR_OUTPUT
		x1Velocity : REAL;
		x2Position : REAL;
		debug : INT;
	END_VAR
	VAR
		lastRef : REAL;
		rampingActive : BOOL;
		RampVelo_0 : RampVelo;
		DisFilter2_0 : DisFilter2;
		firstRun : BOOL;
		zzEdge00000 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK Ramp
	VAR_INPUT
		VelocityMax : REAL;
		in : REAL;
		init : BOOL;
		initRamp : BOOL;
		initPos : REAL;
		RampStartDiff : REAL;
		RampEndDiff : REAL;
	END_VAR
	VAR_OUTPUT
		out : REAL;
	END_VAR
	VAR
		dt : REAL;
		lastIn : REAL;
		RTInfo_0 : RTInfo;
		rampingActive : BOOL;
		rampingDirPos : BOOL;
		factor : REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK RampVelo
	VAR_INPUT
		reference : REAL;
		x1VelocityMax : REAL;
		accMax : REAL;
		init : BOOL;
		initFilter : BOOL;
		initVelo : REAL;
		initPos : REAL;
	END_VAR
	VAR_OUTPUT
		x1Velocity : REAL;
		x2Position : REAL;
		debug : INT;
	END_VAR
	VAR
		RTInfo_0 : RTInfo;
		dt : REAL;
		x1Last : REAL;
		x2Last : REAL;
		ang_limit : REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK DisFilter2
	VAR_INPUT
		uIn : REAL;
		x1VelocityMax : REAL;
		accMax : REAL;
		Ad11 : REAL;
		Ad12 : REAL;
		Ad21 : REAL;
		Ad22 : REAL;
		B1 : REAL;
		B2 : REAL;
		init : BOOL;
		initFilter : BOOL;
		initVelo : REAL;
		initPos : REAL;
		MinLimit : REAL;
		MaxLimit : REAL;
		RunAtHalfFrequency : BOOL;
	END_VAR
	VAR_OUTPUT
		x1Velocity : REAL;
		x2Position : REAL;
		debug : INT;
	END_VAR
	VAR
		x1Last : REAL;
		x2Last : REAL;
		RTInfo_0 : RTInfo;
		dt : REAL;
		MaxVel_ToLimit : REAL;
		MinVel_ToLimit : REAL;
		uIn_limited : REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK Avg6 (*TODO: Add your comment here*) (*$GROUP=User*)
	VAR_INPUT
		enable : ARRAY[0..5] OF BOOL;
		in : ARRAY[0..5] OF REAL;
		lastIndex : USINT;
	END_VAR
	VAR_OUTPUT
		average : REAL;
		minimum : REAL;
		maximum : REAL;
	END_VAR
	VAR
		i : USINT;
		enableCount : USINT;
		averageSum : REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION scaleGain : REAL
	VAR_INPUT
		x : REAL;
		x1 : REAL;
		x2 : REAL;
		y1 : REAL;
		y2 : REAL;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK MotorLevel (*TODO: Add your comment here*) (*$GROUP=User*)
	VAR_INPUT
		level : USINT;
		enable : ARRAY[0..3] OF BOOL;
	END_VAR
	VAR_OUTPUT
		on : ARRAY[0..3] OF BOOL;
	END_VAR
	VAR
		OnTime : ARRAY[0..3] OF UDINT;
		OffTime : ARRAY[0..3] OF UDINT;
		TON_1s : TON;
		i : USINT;
		HighestOnIndex : USINT;
		HighestOffIndex : USINT;
		HighestOffTime : UDINT;
		HighestOnTime : UDINT;
		actualLevel : USINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK LevelStepsWithDelay (*TODO: Add your comment here*) (*$GROUP=User*)
	VAR_INPUT
		in : REAL;
		levelEntry : ARRAY[0..3] OF REAL;
		levelExit : ARRAY[0..3] OF REAL;
		indexMax : USINT;
		WaitTime : TIME;
		InIncreasingValue : BOOL;
	END_VAR
	VAR_OUTPUT
		level : USINT;
	END_VAR
	VAR
		i : USINT;
		levelOld : USINT;
		TOF_SWITCHDELAY : TOF;
		j : USINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK IncreasingWithDelay
	VAR_INPUT
		levelNew : USINT;
		DelayTime : TIME;
	END_VAR
	VAR_OUTPUT
		levelOut : USINT;
	END_VAR
	VAR
		levelOld : USINT;
		TOF_SWITCHDELAY : TOF;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK LevelSteps (*TODO: Add your comment here*) (*$GROUP=User*)
	VAR_INPUT
		in : REAL;
		levelEntry : ARRAY[0..3] OF REAL;
		levelExit : ARRAY[0..3] OF REAL;
		indexMax : USINT;
		InIncreasingValue : BOOL;
	END_VAR
	VAR_OUTPUT
		level : USINT;
	END_VAR
	VAR
		i : USINT;
		j : USINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK LeakageDetector (*TODO: Add your comment here*) (*$GROUP=User*)
	VAR_INPUT
		Initialize : BOOL := FALSE;
		Enable : BOOL := FALSE;
		LeakageTripLevels : ARRAY[0..2] OF REAL;
		LeakageThresholds : ARRAY[0..2] OF REAL;
		ReferenceFilterTime : REAL;
		LeakageFilterTimes : ARRAY[0..2] OF REAL;
		SystemOilMass : REAL;
		Dt : REAL;
		PauseDetection : BOOL;
	END_VAR
	VAR_OUTPUT
		LeakageLevel : ARRAY[0..2] OF BOOL := [3(0)];
	END_VAR
	VAR
		i : INT;
		Initialized : BOOL := FALSE;
		LeakageAccumulator : ARRAY[0..2] OF REAL;
		ReferenceFilter : HCRPT1;
		TON_EvaluationTimer : TON;
		AvgInitValue : REAL := 0;
		TON_LeakageAlarmTimer : ARRAY[0..2] OF TON;
		InitSamples : REAL := 0;
		AccumulatedInput : REAL := 0;
		LeakageFilters : ARRAY[0..2] OF HCRPT1;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK FrequencySensor
	VAR_INPUT
		max_freq : REAL;
		min_freq : REAL;
		max_y : REAL;
		min_y : REAL;
		in : BOOL;
	END_VAR
	VAR_OUTPUT
		out : REAL;
		T : REAL;
	END_VAR
	VAR
		state : USINT;
		count : UINT;
		pos_edge_in : BOOL;
		slope : REAL;
		timer : TON;
		zzEdge00000 : BOOL;
		zzEdge00001 : BOOL;
		t_log_old : DTStructure;
		t_log : DTStructure;
		DTStructureGetTime_1 : DTStructureGetTime;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK PWM_control (*TODO: implement detailed control of PWM control*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		setpoint : {REDUND_UNREPLICABLE} REAL; (*pressure setpoint in bar*)
	END_VAR
	VAR_OUTPUT
		pulseWidth : {REDUND_UNREPLICABLE} INT;
	END_VAR
	VAR_INPUT
		enableCL : BOOL; (*activate closed loop adjustment of pwm pulse width. Only do this when motor is running and relevant valves are activated so that pressure can actually be build up.*)
		measured : REAL; (*measured pressure in bar (for closed-loop adjustment)*)
	END_VAR
	VAR
		CLgain : REAL; (*closed loop gain. Is limited between 0.8 and 1.2*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK createTimeString (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_OUTPUT
		timeString : {REDUND_UNREPLICABLE} STRING[80];
	END_VAR
	VAR
		DTGetTime_1 : DTGetTime;
		DT1 : DATE_AND_TIME;
		MC_sec_string : STRING[2];
		MC_min_string : STRING[2];
		MC_hour_string : STRING[2];
		MC_day_string : STRING[2];
		MC_month_string : STRING[2];
		MC_year_string : STRING[4];
		DT1_structure : DTStructure;
	END_VAR
END_FUNCTION_BLOCK
