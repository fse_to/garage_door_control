(********************************************************************
 * COPYRIGHT --  Eltronic, DK
 ********************************************************************
 * Library: ELT_lib
 * File: EventLog.st
 * Author: rkm
 * Created: June 28, 2010
 ********************************************************************
 * Implementation of library ELT_lib
 ********************************************************************) 

(* Writes an event text to XML *)
FUNCTION EventLog
	tempType := 'Event';
	Args.s[0] := ADR(tempType);
	
	Args.s[1] := ADR(nameText); 
	Args.s[2] := ADR(descText); 
	Args.s[3] := ADR(groupText); 
	Args.i[0] := number;
	Args.r[0] := value1;
	Args.r[1] := value2;
	Args.s[4] := ADR(unit);
	BRSE_ARL_Info(1, 55000, '%s;%s;%s;%s;%i;%r;%r;%s', ADR(Args));

	EventLog := TRUE; // return
END_FUNCTION


(* Writes an alarm text to XML *)
FUNCTION AlarmLog
	IF newState THEN
		tempType := 'Set';
	ELSE
		tempType := 'Reset';
	END_IF
	Args.s[0] := ADR(tempType);
	Args.i[0] := number;
	Args.s[1] := ADR(nameText); 
	
	BRSE_ARL_Fatal(1, 50000+number, 'Alarm %s %i: %s', ADR(Args));
	AlarmLog := TRUE; // return
END_FUNCTION

(* Writes an user login/logout text to XML *)
FUNCTION UserLog
	
	IF logAction THEN
		
		Args.s[0] := ADR('User Logout');
		Args.s[1] := ADR(username);
		Args.i[0] := sessionDuration;
		
		
		BRSE_ARL_Info(2, 80000, '%s, Name: %s, Time: %i min', ADR(Args));
		
	ELSE
		Args.s[0] := ADR('User Login');
		Args.s[1] := ADR(username);
		
		BRSE_ARL_Info(2, 80000, '%s, Name: %s', ADR(Args));
	END_IF
	
	UserLog := TRUE; // return
END_FUNCTION


(* Debug Text Log *)
FUNCTION DebugLog
	tempType := 'Debug';
	Args.s[0] := ADR(tempType);
	
	Args.s[1] := ADR(nameText); 
	Args.s[2] := ADR(descText); 
	Args.s[3] := ADR(groupText); 
	Args.i[0] := number;
	Args.r[0] := value1;
	Args.r[1] := value2;
	Args.s[4] := ADR(unit);
	BRSE_ARL_Info(1, 57000, '%s;%s;%s;%s;%i;%r;%r;%s', ADR(Args));

	DebugLog := TRUE;  // return
END_FUNCTION
