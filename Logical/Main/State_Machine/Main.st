
PROGRAM _INIT
	
	this ACCESS ADR(gMain); 
	
	FOR i:=0 TO 1 DO
		RefFilter[i].accMax			:= 20; 			// [mm/s^2]
		RefFilter[i].MaxLimit 		:= max_stroke; 	// [mm]
		RefFilter[i].MinLimit 		:= min_stroke;  // [mm]
		RefFilter[i].x1VelocityMax 	:= 15.0; 		// [mm/s]
				
		// Filter coefficients for execution at 10 Hz.
		// 10 [rad/s]
		RefFilter[i].Ad11 	:= 0;
		RefFilter[i].Ad12 	:= -3.6788;
		RefFilter[i].Ad21 	:= 0.0368; 
		RefFilter[i].Ad22 	:= 0.7358;
		RefFilter[i].B1 	:= 3.6788; 
		RefFilter[i].B2 	:= 0.2642;
		
		// 2 [rad/s]
		RefFilter[i].Ad11 	:= 0.6550;
		RefFilter[i].Ad12 	:= -0.3275;
		RefFilter[i].Ad21 	:= 0.0819; 
		RefFilter[i].Ad22 	:= 0.9825;
		RefFilter[i].B1 	:= 0.3275; 
		RefFilter[i].B2 	:= 0.0175;

		RefFilter[i].RunAtHalfFrequency  := FALSE;
	END_FOR	
		
	RefFilter[0](init :=TRUE , initFilter :=TRUE , initPos :=this.io.ain.Sensor1.v );
	RefFilter[1](init :=TRUE , initFilter :=TRUE , initPos :=this.io.ain.Sensor2.v );
		
	sensor1_filtered(enable:=TRUE, V:= 1, T1 := 2, Set_y := 2, y_set := 0.0);
	sensor2_filtered(enable:=TRUE, V:= 1, T1 := 2, Set_y := 2, y_set := 0.0);
	
END_PROGRAM

PROGRAM _CYCLIC
	
	// Pointer to object
	this ACCESS ADR(gMain); 
	
	// IO scaling: [mA] -> [mm]	
	this.io.ain.Sensor1(scale4mA := 0, scale20mA := 1500); 
	this.io.ain.Sensor2(scale4mA := 0, scale20mA := 1500); 	

	sensor2_filtered(T1 := 2,u:=this.io.ain.Sensor2.v); 
	
	IF stopped THEN
		sensor1_filtered(T1 := 2,u:=this.io.ain.Sensor1.v); 
		sensor1old := this.io.ain.Sensor1.v;
	ELSE // Running
		IF ABS(sensor1old - this.io.ain.Sensor1.v) > 20 THEN // sensor1: FAILED (20[mm] jump in 1 sample)
			fake_sensor1_input := sensor1old + (sensor2_filtered.y - sensor2_old); // sensor 1 follows changes of sensor 2. 
			sensor1_filtered(T1 := 2,u := fake_sensor1_input ); // add the distance that sensor 2 have moved to sensor 1, while it is not working. 
			sensor1old := fake_sensor1_input;
		ELSE // sensor1: OK
			sensor1_filtered(T1 := 2,u:=this.io.ain.Sensor1.v); 
			sensor1old := this.io.ain.Sensor1.v;
		END_IF
	END_IF
	
	sensor2_old := sensor2_filtered.y;
	
	// Delay command inputs before moving the port
	UpTimer(IN:=this.io.din.Open, PT:= T#4s);
	DownTimer(IN:=this.io.din.Close, PT:= T#4s); 
		
	
	IF UpTimer.Q THEN // Port commanded to open
		stopped 					:= FALSE; 
		RefFilter[0].uIn			:= max_stroke; 
		RefFilter[1].uIn			:= max_stroke; 		
		RefFilter[0].x1VelocityMax 	:= 18.0; // [mm/s]
		RefFilter[1].x1VelocityMax 	:= 18.0; // [mm/s]
		
	ELSIF DownTimer.Q  THEN // Port commanded to close
		stopped 					:= FALSE; 
		RefFilter[0].uIn			:= min_stroke; 
		RefFilter[1].uIn			:= min_stroke;
		
	ELSE // Port commanded to stop
		stopped := TRUE; 
		RefFilter[0](initFilter := TRUE, initPos := (sensor1_filtered.y + sensor2_filtered.y)/2, initVelo := 0); 
		RefFilter[1](initFilter := TRUE, initPos := (sensor1_filtered.y + sensor2_filtered.y)/2, initVelo := 0); 
	END_IF
	
	// Filter the references
	FOR i := 0 TO 1 DO
		RefFilter[i]();	
	END_FOR	
	
	// Get reference (for logging purposes)
	this.ReferencePos1 := RefFilter[0].x2Position;
	this.ReferencePos2 := RefFilter[1].x2Position;

	// Control error
	Error1	:= (RefFilter[0].x2Position - sensor1_filtered.y);
	Error2	:= (RefFilter[1].x2Position - sensor2_filtered.y); 
	
	// Proportional part
	this.CS1_P := Error1*p_gain;
	this.CS2_P := Error2*p_gain;
	
	// Integrator part
	IF stopped THEN
		IntLimited_1 := 0; 
		IntLimited_2 := 0; 
	ELSE
		IntLimited_1 := LIMIT(-1000,IntLimited_1 + Error1,1000); 
		IntLimited_2 := LIMIT(-1000,IntLimited_2 + Error2,1000); 
	END_IF
		
	CS_INT1 := IntLimited_1 * I_gain; 
	CS_INT2 := IntLimited_2 * I_gain; 
	
	// Feedforward part
	this.FF1 := RefFilter[0].x1Velocity*FF_gain; 
	this.FF2 := RefFilter[1].x1Velocity*FF_gain; 

	// Limit control signals in range: {-100[%]..+100[%]}
	this.CS1 := LIMIT(-100,this.FF1 + this.CS1_P + CS_INT1,100); // [%]
	this.CS2 := LIMIT(-100,this.FF2 + this.CS2_P + CS_INT2,100); // [%]
	
	// Sensor error if the deviation betwen sensors is above 40[mm]
	IF ABS(this.io.ain.Sensor1.v - this.io.ain.Sensor2.v) > 40 THEN
		sensor_error_1 := TRUE; 
	ELSE
		sensor_error_1 := FALSE; 
	END_IF
	
	// Sensor error if the deviation betwen sensors is above 40[mm]
	IF ABS(sensor1_filtered.y - sensor2_filtered.y) > 40 THEN
		sensor_error_2 := TRUE; 
	ELSE
		sensor_error_2 := FALSE; 
	END_IF
	
	// Trigger alarm if sensor errors persist during 2[s] and 1[s]
	AlarmTimer1(IN:=sensor_error_1,PT:= T#2s); 
	AlarmTimer2(IN:=sensor_error_2,PT:= T#1s); 

	// Stop cylinders by closing p-valves if an alarm is triggered
	IF AlarmTimer1.Q OR AlarmTimer2.Q THEN 
		this.CS1 := 0; // close p-valve of cyl. 1
		this.CS2 := 0; // close p-valve of cyl. 2
	END_IF
	
	// Deadband compensation
	IF this.CS1 > 0 THEN
		this.CS1 := this.CS1 + 0;
	ELSE
		this.CS1 := this.CS1 - 0;
	END_IF
	
	IF this.CS2 > 0 THEN
		this.CS2 := this.CS2 + 0;
	ELSE
		this.CS2 := this.CS2 - 0;
	END_IF
	
	this.CS1 := LIMIT(-100,this.CS1,100); 
	this.CS2 := LIMIT(-100,this.CS2,100); 
	
	// IO scaling {-100[%]..+100[%]} -> {0..32768}
	this.io.aout.Cmd1 := REAL_TO_INT( this.CS1/100*32768/2+32768/2 ); 
	this.io.aout.Cmd2 := REAL_TO_INT( this.CS2/100*32768/2+32768/2 ); 	
		
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

