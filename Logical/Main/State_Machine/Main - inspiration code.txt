// Define reference filter coefficients
// Filter coefficients for execution at 100 Hz. 
RefFilter[i].Ad11 := 0.8144;
RefFilter[i].Ad12 := -0.9048;
RefFilter[i].Ad21 := 0.0090; 
RefFilter[i].Ad22 := 0.9953;
RefFilter[i].B1   := 0.9048; 
RefFilter[i].B2   := 0.0047;


// Close port slowly (at the beginning) and increase the velocity afterwards
IF RefFilter[0].x2Position > 1200 THEN 
	RefFilter[0].x1VelocityMax := 10.0; // [mm/s] 
	RefFilter[1].x1VelocityMax := 10.0; // [mm/s] 		
ELSE
	RefFilter[0].x1VelocityMax := 15.0; // [mm/s] 
	RefFilter[1].x1VelocityMax := 15.0; // [mm/s] 
END_IF
