
TYPE
	result_typ : 	STRUCT 
		final : STRING[1000];
		time : createTimeString;
		realtemp1 : ARRAY[0..59]OF REAL;
		realtemp0 : ARRAY[0..59]OF REAL;
		strtemp : ARRAY[0..19]OF strtemp_typ;
	END_STRUCT;
	main_typ : 	STRUCT 
		command : command_typ;
		status : status_typ;
		io : io_typ;
		hmi : hmi_typ;
		CS1_P : REAL;
		CS2_P : REAL;
		FF1 : REAL;
		FF2 : REAL;
		CS1 : REAL;
		CS2 : REAL;
		ReferencePos1 : REAL;
		ReferencePos2 : REAL;
		result : result_typ;
	END_STRUCT;
	command_typ : 	STRUCT 
		resetSafety : BOOL;
		serialChange : BOOL;
		manualMode : BOOL;
		btn25 : BOOL;
		btn64 : BOOL;
		btn211 : BOOL;
		btn253 : BOOL;
		btn216 : BOOL;
		btnV1 : BOOL;
		btnV2 : BOOL;
		btnV4_V6 : BOOL;
		btnV5_V7 : BOOL;
		btnMain : BOOL;
		testRun : BOOL;
		testStop : BOOL;
		v3Setpoint : REAL;
		testTimerExtra : TON;
		testTimer : TON;
		step : USINT := 0;
	END_STRUCT;
	status_typ : 	STRUCT 
		state : UINT;
		substate : UINT;
	END_STRUCT;
	aout_typ : 	STRUCT 
		Cmd1 : INT;
		Cmd2 : INT;
	END_STRUCT;
	io_typ : 	STRUCT 
		ain : ain_typ;
		din : din_typ;
		aout : aout_typ;
	END_STRUCT;
	ain_typ : 	STRUCT 
		Sensor1 : SCALE;
		Sensor2 : SCALE;
	END_STRUCT;
	din_typ : 	STRUCT 
		Close : BOOL;
		Open : BOOL;
	END_STRUCT;
	hmi_typ : 	STRUCT 
		btn25 : UINT; (*Button State - Used for HMI Colormap*)
		btn64 : UINT;
		btn253 : UINT;
		btn211 : UINT;
		btn216 : UINT;
		btnV1 : UINT;
		btnV2 : UINT;
		btnV4_V6 : UINT;
		btnV5_V7 : UINT;
		btnMain : UINT;
		serialNumber : STRING[10];
		currentPage : USINT;
		changePage : USINT;
		operator : STRING[10];
		hidePopUpOk : USINT;
		hidePopUpYesNo : USINT;
		btnOK : BOOL;
		btnNo : BOOL;
		btnYes : BOOL;
		popupmessageLine1 : STRING[49];
		popupmessageLine2 : STRING[49];
		popupmessageLine3 : STRING[49];
		popupmessageLine4 : STRING[49];
		popupmessageLine5 : STRING[49];
		hideRun : USINT;
		ChineseOn : BOOL;
	END_STRUCT;
	strtemp_typ : 	STRUCT 
		strtemp : STRING[5];
	END_STRUCT;
END_TYPE
