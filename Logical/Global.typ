
TYPE
	alarm_typ : 	STRUCT 
		alarmActive : BOOL;
		alarms : ARRAY[0..MAX_ALARMS]OF BOOL;
		hideAlarm : UINT;
		hideReset : UINT;
	END_STRUCT;
	retain_typ : 	STRUCT 
		P1_hwoffset : INT;
	END_STRUCT;
END_TYPE
