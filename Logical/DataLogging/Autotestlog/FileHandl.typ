(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: FileHandling
 * File: FileHandling.typ
 * Author: Bernecker + Rainer
 * Created: April 16, 2009
 ********************************************************************
 * Local data types of program FileHandling
 ********************************************************************)

TYPE
	FileHandling_Functionblock_typ : 	STRUCT 
		DevLink_0 : DevLink; (*Functionblock DevLink()*)
		FileCreate_0 : FileCreate; (*Functionblock FileCreate()*)
		FileClose_0 : FileClose; (*Functionblock FileClose()*)
		FileOpen_0 : FileOpen; (*Functionblock FileOpen()*)
		FileRead_0 : FileRead; (*Functionblock FileWrite()*)
		FileWrite_0 : FileWrite; (*Functionblock FileWrite()*)
		FileDelete_0 : FileDelete; (*Functionblock FileDelete()*)
		DeviceMemory_0 : DevMemInfo; (*Functionblock DevMemInfo()*)
		FileCompress_0 : zipArchive;
		DevLink_1 : DevLink; (*Functionblock DevLink()*)
		FileCreate_1 : FileCreate; (*Functionblock FileCreate()*)
		FileClose_1 : FileClose; (*Functionblock FileClose()*)
		FileOpen_1 : FileOpen; (*Functionblock FileOpen()*)
		FileWrite_1 : FileWrite; (*Functionblock FileWrite()*)
		FileDelete_1 : FileDelete; (*Functionblock FileDelete()*)
		DeviceMemory_1 : DevMemInfo; (*Functionblock DevMemInfo()*)
		FileCompress_1 : zipArchive;
		DirRead_0 : DirRead;
		DirCreate_0 : DirCreate;
		DirDelete_0 : DirDelete;
	END_STRUCT;
	FileHandling_Data_typ : 	STRUCT 
		FileName0 : STRING[80]; (*filename for command bCreateFile, bDeleteFile, bWriteFile, bReadFile, bReadExFile, bCopyFile, bRenameFile*)
		Device : STRING[32]; (*device name*)
		Parameter : STRING[80]; (*parameterstring for FUB DevLink()*)
		ReadData0 : STRING[800000]; (*data for command bWriteFile*)
		WriteData0 : STRING[1000]; (*data for command bWriteFile*)
		Step : USINT; (*step variable (255 = error step)*)
	END_STRUCT;
	FileHandling_Command_typ : 	STRUCT 
		bCreateFile : BOOL; (*Command: create new file*)
		bWriteFile : BOOL; (*Command: write data into file*)
		bDeleteFile : BOOL; (*Command: detete a file*)
	END_STRUCT;
	FileHandling_typ : 	STRUCT 
		Command : FileHandling_Command_typ; (*Command structure*)
		Data : FileHandling_Data_typ; (*Data structure*)
		Functionblock : FileHandling_Functionblock_typ; (*FUB structure*)
	END_STRUCT;
END_TYPE
