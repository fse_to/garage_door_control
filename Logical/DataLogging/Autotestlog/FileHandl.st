(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: FileHandling
 * File: FileHandling.st
 * Author: Bernecker + Rainer
 * Created: April 16, 2009
 ********************************************************************
 * Implementation of program Handling
 ********************************************************************)

(******************************************)
(* init program                           *)
(******************************************)
PROGRAM _INIT

//	showtime.enable := 0;
//	showtime.DT1 := DT#2020-28-20-08:23;  (* actual time *)
//	showtime();                           (* set time *)

		(* device name for different function blocks needed (choose yourself) *)
		strcpy( ADR(Handling.Data.Device), ADR('USB_AUTO_DEVICE') );
		(* perameter string, layout see in the online help by function block "devlink" *)
		strcpy( ADR(Handling.Data.Parameter), ADR('"/DEVICE=/bd0') ); 
		// You need to use the AsUSB library to find out about USB device name. See example ST1 in the AsUSB library.

	
		(* Name the file *);
		strcpy( ADR(Handling.Data.FileName0), ADR('D7log.csv') );
	
//	Handling.Command.bCreateFile := 1;
END_PROGRAM

(******************************************)
(* cyclic program                         *)
(******************************************)
PROGRAM _CYCLIC
	
	CASE Handling.Data.Step OF		

		0:  (* link (create) a file device *)
			Fifoglobal_autotest.HMI_display := 'Preparing USB';
			Handling.Functionblock.DevLink_0.enable := 1;
			Handling.Functionblock.DevLink_0.pDevice := ADR(Handling.Data.Device);  (* name of the device, which is needed for other functionblocks *)
			Handling.Functionblock.DevLink_0.pParam := ADR(Handling.Data.Parameter);  (* devicepath *)
			Handling.Functionblock.DevLink_0;  (* call the function*)
			
			IF Handling.Functionblock.DevLink_0.status = 0 THEN  (* DevLink successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Fifoglobal_autotest.HMI_display := 'USB Ready';
				Fifoglobal_autotest.Filewrite_ready := TRUE;
			ELSIF Handling.Functionblock.DevLink_0.status = ERR_FUB_BUSY THEN  (* DevLink not finished -> redo *)			
				(* Busy *)	
			ELSIF Handling.Functionblock.DevLink_0.status = fiERR_SYSTEM THEN  (* DevLink error = fiERR_SYSTEM -> detailed info with function "FileIoGetSysError" *)			
				(* get detail errorinformation *)
				Error := FileIoGetSysError();	
				(* Goto Error Step *)
				Handling.Data.Step := 255;	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.DevLink_0.status;
			END_IF	
		
		1:  (* Wait and comannd step *)
			
			IF Fifoglobal_autotest.Do_filewrite THEN
				Handling.Data.Step := 19;
				Fifoglobal_autotest.Do_filewrite := FALSE;
				Fifoglobal_autotest.HMI_display := 'Writing to USB';
				
				(* Data *)
				strcpy(ADR(Handling.Data.WriteData0),ADR(Fifoglobal_autotest.strdata));
				
				(* Name the file *);
				strcpy( ADR(Handling.Data.FileName0), ADR(Fifoglobal_autotest.filename) );
			END_IF
			
			
			IF Handling.Command.bCreateFile = 1 THEN
				(* Create a new file with the name from the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 9;  (* next Step*)
		
			ELSIF Handling.Command.bWriteFile = 1 THEN
				(* writes the data from variable "Handling.Data.WriteData" 
				   in the file with the name from the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 20;  (* next Step*)  
															
			ELSIF Handling.Command.bDeleteFile = 1 THEN
				(* deletes the file with the name from the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 70;  (* next Step*)  
			END_IF
		
			(***********************************************************************************************)
		9: (* Check the memory status of the device to see if it should be emptied for files soon *) 
			Handling.Functionblock.DeviceMemory_0.enable := 1;
			Handling.Functionblock.DeviceMemory_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.DeviceMemory_0; (* Call function *) 
			
			IF Handling.Functionblock.DeviceMemory_0.status = 0 THEN  (* DeviceMemory successful *)
				
				IF Handling.Functionblock.DeviceMemory_0.freemem > 20000000 THEN (* If their is free space left (more than 20 MB), then create a new file *)
					MEMORY_IS_FULL_EMPTY_USB := FALSE; 
					Handling.Data.Step := 10;  (* next Step, create new file *)
				ELSE (* If their is no free space left, then do not create a new file *) 
					MEMORY_IS_FULL_EMPTY_USB := TRUE;
				END_IF
				
			ELSIF Handling.Functionblock.DeviceMemory_0.status = ERR_FUB_BUSY THEN  (* DeviceMemory not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.DeviceMemory_0.status;
			END_IF	
		
		10:  (* create a new File with the selected name  *)
			Handling.Functionblock.FileCreate_0.enable := 1;
			Handling.Functionblock.FileCreate_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.FileCreate_0.pFile := ADR(Handling.Data.FileName0);  (* name of the file *)
			Handling.Functionblock.FileCreate_0;  (* call the function*)
			
			IF Handling.Functionblock.FileCreate_0.status = 0 THEN  (* FileCreate successful *)
				Handling.Data.Step := 11;  (* next Step*)
				// Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bCreateFile := 0;  (* clear command *)	
			ELSIF Handling.Functionblock.FileCreate_0.status = ERR_FUB_BUSY THEN  (* FileCreate not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileCreate_0.status;
			END_IF	
			
		11:	 (* close file, because of limited number of available file handles on the system *)
			Handling.Functionblock.FileClose_0.enable := 1;
			Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileCreate_0.ident; (* ident for FileCreate-functionblock*)
			Handling.Functionblock.FileClose_0;  (* call the function*)
			
			IF Handling.Functionblock.FileClose_0.status = 0 THEN  (* FileClose successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bCreateFile := 0;  (* clear command *)
			ELSIF Handling.Functionblock.FileClose_0.status = ERR_FUB_BUSY THEN  (* FileClose not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileClose_0.status;
			END_IF	

			(***********************************************************************************************)	
		19: (* Check the memory status of the device to see if it should be emptied for files soon *) 
			Handling.Functionblock.DeviceMemory_0.enable := 1;
			Handling.Functionblock.DeviceMemory_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.DeviceMemory_0; (* Call function *) 
			
			IF Handling.Functionblock.DeviceMemory_0.status = 0 THEN  (* DeviceMemory successful *)
				
				IF Handling.Functionblock.DeviceMemory_0.freemem > 20000000 THEN (* If their is free space left (more than 20 MB), then create a new file *)
					MEMORY_IS_FULL_EMPTY_USB := FALSE; 
					Handling.Data.Step := 20;  (* next Step, create new file *)
				ELSE (* If their is no free space left, then do not create a new file *) 
					MEMORY_IS_FULL_EMPTY_USB := TRUE;
				END_IF
				
			ELSIF Handling.Functionblock.DeviceMemory_0.status = ERR_FUB_BUSY THEN  (* DeviceMemory not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.DeviceMemory_0.status;
			END_IF	
			
		20:	(* open file 0 for write access *)
			Handling.Functionblock.FileOpen_0.enable := 1;
			Handling.Functionblock.FileOpen_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.FileOpen_0.pFile :=  ADR(Handling.Data.FileName0);  (* name of the file *)
			Handling.Functionblock.FileOpen_0.mode := fiREAD_WRITE; (* write access *)
			Handling.Functionblock.FileOpen_0;  (* call the function*)

			IF Handling.Functionblock.FileOpen_0.status = 0 THEN  (* FileOpen successful *)
				Handling.Data.Step := 22;  (* next Step*)
			ELSIF Handling.Functionblock.FileOpen_0.status = ERR_FUB_BUSY THEN  (* FileOpen not finished -> redo *)			
				(* Busy *)	
			ELSIF Handling.Functionblock.FileOpen_0.status = 20708 THEN (* File is missing *)
				Handling.Data.Step := 31;				(* Create File *) 
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileOpen_0.status;
			END_IF	
			
		22: (* write data into file *)
			Handling.Functionblock.FileWrite_0.enable := 1;
			Handling.Functionblock.FileWrite_0.ident := Handling.Functionblock.FileOpen_0.ident;
			Handling.Functionblock.FileWrite_0.offset := Handling.Functionblock.FileOpen_0.filelen; //(ADR(Handling.Data.ReadData0));
			Handling.Functionblock.FileWrite_0.pSrc := ADR(Handling.Data.WriteData0);
			Handling.Functionblock.FileWrite_0.len := strlen(ADR(Handling.Data.WriteData0));
			Handling.Functionblock.FileWrite_0;  (* call the function*)

			IF Handling.Functionblock.FileWrite_0.status = 0 THEN  (* FileWrite successful *)
				Handling.Data.Step := 23;  (* next Step*)
				(*strcpy(ADR(Handling.Data.ReadData0),ADR(''));
				strcpy(ADR(Handling.Data.WriteData0),ADR(''));
				Handling.Functionblock.FileWrite_0.offset := 0;
				Handling.Functionblock.FileWrite_0.ident := 0;
				Handling.Functionblock.FileWrite_0.pSrc := 0; *)
			ELSIF Handling.Functionblock.FileWrite_0.status = ERR_FUB_BUSY THEN  (* FileWrite not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileWrite_0.status;
			END_IF	
			
		23:	 (* close file, because of limited number of available file handles on the system *)
			Handling.Functionblock.FileClose_0.enable := 1;
			Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileOpen_0.ident; (* ident for FileCreate-functionblock*)
			//	Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileCreate_0.ident; (* ident for FileCreate-functionblock*)
			Handling.Functionblock.FileClose_0;  (* call the function*)
			
			IF Handling.Functionblock.FileClose_0.status = 0 THEN  (* FileClose successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bWriteFile := 0;  (* clear command *)
				Fifoglobal_autotest.HMI_display := 'Writing Done';
				Fifoglobal_autotest.Filewrite_ready := TRUE;
			ELSIF Handling.Functionblock.FileClose_0.status = ERR_FUB_BUSY THEN  (* FileClose not finished -> redo *)			
				(* Busy *)
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileClose_0.status;
			END_IF	

			(***********************************************************************************************)
			
		30: (* Check the memory status of the device to see if it should be emptied for files soon *) 
			Handling.Functionblock.DeviceMemory_0.enable := 1;
			Handling.Functionblock.DeviceMemory_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.DeviceMemory_0; (* Call function *) 
			
			IF Handling.Functionblock.DeviceMemory_0.status = 0 THEN  (* DeviceMemory successful *)
				
				IF Handling.Functionblock.DeviceMemory_0.freemem > 20000000 THEN (* If their is free space left (more than 20 MB), then create a new file *)
					MEMORY_IS_FULL_EMPTY_USB := FALSE; 
					Handling.Data.Step := 31;  (* next Step, create new file *)
				ELSE (* If their is no free space left, then do not create a new file *) 
					MEMORY_IS_FULL_EMPTY_USB := TRUE;
					Fifoglobal_autotest.HMI_display := 'USB MEMORY FULL';
				END_IF
				
			ELSIF Handling.Functionblock.DeviceMemory_0.status = ERR_FUB_BUSY THEN  (* DeviceMemory not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.DeviceMemory_0.status;
			END_IF	
		
		31:  (* create a new File 0 with the selected name  *)
			Handling.Functionblock.FileCreate_0.enable := 1;
			Handling.Functionblock.FileCreate_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.FileCreate_0.pFile := ADR(Handling.Data.FileName0);  (* name of the file *)
			Handling.Functionblock.FileCreate_0;  (* call the function*)
			
			IF Handling.Functionblock.FileCreate_0.status = 0 THEN  (* FileCreate successful *)
				Handling.Data.Step := 32;  (* next Step*)	
			//	strcpy(ADR(Handling.Data.ReadData0),ADR(''));
			ELSIF Handling.Functionblock.FileCreate_0.status = ERR_FUB_BUSY THEN  (* FileCreate not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileCreate_0.status;
			END_IF	
		32:	 (* close file, because of limited number of available file handles on the system *)
			Handling.Functionblock.FileClose_0.enable := 1;
			Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileCreate_0.ident; (* ident for FileCreate-functionblock*)
			Handling.Functionblock.FileClose_0;  (* call the function*)
			
			IF Handling.Functionblock.FileClose_0.status = 0 THEN  (* FileClose successful *)
				Handling.Data.Step := 20;  (* next Step*)
			ELSIF Handling.Functionblock.FileClose_0.status = ERR_FUB_BUSY THEN  (* FileClose not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileClose_0.status;
			END_IF		
		
			(***********************************************************************************************)	
		70: (* delete a file *)
		
			Handling.Functionblock.FileDelete_0.enable := 1;
			Handling.Functionblock.FileDelete_0.pDevice := ADR(Handling.Data.Device);  (* name of a linked device *)
			Handling.Functionblock.FileDelete_0.pName := ADR(Handling.Data.FileName0);  (* name of the source file *)
			Handling.Functionblock.FileDelete_0;  (* call the function*)
			
			IF Handling.Functionblock.FileDelete_0.status = 0 THEN  (* FileRename successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bDeleteFile := 0;  (* clear command *)
			ELSIF Handling.Functionblock.FileDelete_0.status = ERR_FUB_BUSY THEN  (* FileDelete not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileDelete_0.status;
			END_IF

			(***********************************************************************************************)	
		255:  (* Here some error Handling has to be implemented *)	
			//	Handling.Data.Step := 1;
			Fifoglobal_autotest.HMI_display := 'USB Error ';
			itoa(Error,ADR(Error_string));
			strcat(ADR(Fifoglobal_autotest.HMI_display),ADR(Error_string));
			Fifoglobal_autotest.Filewrite_ready := FALSE;
		
			IF EDGEPOS(Fifoglobal_autotest.HMI_clear_log) THEN
				Handling.Data.Step := 1;
				Fifoglobal_autotest.HMI_clear_log := 0;
				Fifoglobal_autotest.blockTestButton3 := 0;
				Fifoglobal_autotest.HMI_display := 'USB Ready ';
				Fifoglobal_autotest.Filewrite_ready := TRUE;
			END_IF
				
	END_CASE
	

	
END_PROGRAM
