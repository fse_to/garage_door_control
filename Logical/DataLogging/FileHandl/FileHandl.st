(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: FileHandling
 * File: FileHandling.st
 * Author: Bernecker + Rainer
 * Created: April 16, 2009
 ********************************************************************
 * Implementation of program Handling
 ********************************************************************)

(******************************************)
(* init program                           *)
(******************************************)
PROGRAM _INIT

//	showtime.enable := 1;
//	showtime.DT1 := DT#2018-12-20-08:23;  (* actual time *)
//	showtime();                           (* set time *)

		(* device name for different function blocks needed (choose yourself) *)
		strcpy( ADR(Handling.Data.Device), ADR('USB_DEVICE') );
		(* perameter string, layout see in the online help by function block "devlink" *)
		strcpy( ADR(Handling.Data.Parameter), ADR('"/DEVICE=/bd0') ); 
		// You need to use the AsUSB library to find out about USB device name. See example ST1 in the AsUSB library.

	
	(* predefine filenames *)
(*	MC_year := 2018;
	MC_month := 11;
	MC_day := 16;
	MC_hour := 10;
	MC_min := 0;
	MC_sec := 6; *) 
	
	//DT1:=TIME_TO_DT(clock_ms()); 
	DTGetTime_1.enable:=1;
	DTGetTime_1();
	DT1:=DTGetTime_1.DT1;  
	DT_TO_DTStructure(DT1,ADR(DT1_structure));

	itoa(DT1_structure.year,ADR(MC_year_string));
	itoa(DT1_structure.month,ADR(MC_month_string));
	itoa(DT1_structure.day,ADR(MC_day_string));
	itoa(DT1_structure.hour,ADR(MC_hour_string));
	itoa(DT1_structure.minute,ADR(MC_min_string));
	itoa(DT1_structure.second,ADR(MC_sec_string));
	
	strcpy( ADR(Handling.Data.FileName), ADR(gMain.hmi.serialNumber) );
	strcat(ADR(Handling.Data.FileName),ADR('_'));
	strcat(ADR(Handling.Data.FileName),ADR(MC_year_string));
	strcat(ADR(Handling.Data.FileName),ADR('_'));
	strcat(ADR(Handling.Data.FileName),ADR(MC_month_string));
	strcat(ADR(Handling.Data.FileName),ADR('_'));
	strcat(ADR(Handling.Data.FileName),ADR(MC_day_string));
	strcat(ADR(Handling.Data.FileName),ADR('_'));
	strcat(ADR(Handling.Data.FileName),ADR(MC_hour_string));
	strcat(ADR(Handling.Data.FileName),ADR('_'));
	strcat(ADR(Handling.Data.FileName),ADR(MC_min_string));
	strcat(ADR(Handling.Data.FileName),ADR('_'));
	strcat(ADR(Handling.Data.FileName),ADR(MC_sec_string));
	
	strcpy(ADR(Handling.Data.FileName_old),ADR(Handling.Data.FileName)); (*save old filename for compression step and delete step*)
	strcpy(ADR(Handling.Data.FileName_old_gz),ADR(Handling.Data.FileName)); (*save old filename for compression step and delete step*)
	strcat(ADR(Handling.Data.FileName_old_gz),ADR('.gz')); (* Add file extension *) 
	
	Fifoglobal.Filewrite_ready := 0;
	Fifoglobal.Do_filewrite := 0;
	writeoffset := 0;
	counter := 0;
	
	Handling.Command.bCreateFile := 0;
END_PROGRAM

(******************************************)
(* cyclic program                         *)
(******************************************)
PROGRAM _CYCLIC
	
	CASE Handling.Data.Step OF		

		0:  (* link (create) a file device *)
			Handling.Functionblock.DevLink_0.enable := 1;
			Handling.Functionblock.DevLink_0.pDevice := ADR(Handling.Data.Device);  (* name of the device, which is needed for other functionblocks *)
			Handling.Functionblock.DevLink_0.pParam := ADR(Handling.Data.Parameter);  (* devicepath *)
			Handling.Functionblock.DevLink_0;  (* call the function*)
			
			IF Handling.Functionblock.DevLink_0.status = 0 THEN  (* DevLink successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Fifoglobal.Filehandling.Device_ready := 1; (* Report to main script that device is ready *) 
			ELSIF Handling.Functionblock.DevLink_0.status = ERR_FUB_BUSY THEN  (* DevLink not finished -> redo *)			
				(* Busy *)	
			ELSIF Handling.Functionblock.DevLink_0.status = fiERR_SYSTEM THEN  (* DevLink error = fiERR_SYSTEM -> detailed info with function "FileIoGetSysError" *)			
				(* get detail errorinformation *)
				Error := FileIoGetSysError();	
				(* Goto Error Step *)
				Handling.Data.Step := 255;	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.DevLink_0.status;
			END_IF	
		
		1:  (* Wait and comannd step *)
			
			IF (TRUE = Fifoglobal.Newfifo) THEN
				
				IF Fifoglobal.reset = TRUE THEN
					counter := 0;
					writeoffset := 0;
					
					//DT1:=TIME_TO_DT(clock_ms()); 
					DTGetTime_1.enable:=1;
					DTGetTime_1();
					DT1:=DTGetTime_1.DT1; 
					DT_TO_DTStructure(DT1,ADR(DT1_structure));

					itoa(DT1_structure.year,ADR(MC_year_string));
					itoa(DT1_structure.month,ADR(MC_month_string));
					itoa(DT1_structure.day,ADR(MC_day_string));
					itoa(DT1_structure.hour,ADR(MC_hour_string));
					itoa(DT1_structure.minute,ADR(MC_min_string));
					itoa(DT1_structure.second,ADR(MC_sec_string));
	
					strcpy(ADR(Handling.Data.FileName_old),ADR(Handling.Data.FileName)); (*save old filename for compression step and delete step*)
					strcpy(ADR(Handling.Data.FileName_old_gz),ADR(Handling.Data.FileName)); (*save old filename for compression step and delete step*)
					strcat(ADR(Handling.Data.FileName_old_gz),ADR('.gz')); (* Add file extension *) 
					
					strcpy( ADR(Handling.Data.FileName), ADR(gMain.hmi.serialNumber) );
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_year_string));
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_month_string));
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_day_string));
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_hour_string));
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_min_string));
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_sec_string));
					
					Handling.Data.Step := 30;
					
					Fifoglobal.reset := 0;
					
				ELSIF counter = 180 THEN (* After 1 hour, it is time to make a new file. *)
					
					(* reset *)
					counter := 0;
					writeoffset := 0;
					Handling.Data.Step := 30; (* Step for initiating creating a new file and compress old file *)
	
					(* Make new filename from the time and date *)
					//DT1:=TIME_TO_DT(clock_ms()); 
					DTGetTime_1.enable:=1;
					DTGetTime_1();
					DT1:=DTGetTime_1.DT1; 
					DT_TO_DTStructure(DT1,ADR(DT1_structure));
					
					itoa(DT1_structure.year,ADR(MC_year_string));
					itoa(DT1_structure.month,ADR(MC_month_string));
					itoa(DT1_structure.day,ADR(MC_day_string));
					itoa(DT1_structure.hour,ADR(MC_hour_string));
					itoa(DT1_structure.minute,ADR(MC_min_string));
					itoa(DT1_structure.second,ADR(MC_sec_string));
					
					strcpy(ADR(Handling.Data.FileName_old),ADR(Handling.Data.FileName)); (*save old filename for compression step and delete step*)
					strcpy(ADR(Handling.Data.FileName_old_gz),ADR(Handling.Data.FileName)); (*save old filename for compression step and delete step*)
					strcat(ADR(Handling.Data.FileName_old_gz),ADR('.gz')); (* Add file extension *) 
					
					strcpy( ADR(Handling.Data.FileName), ADR(gMain.hmi.serialNumber) );
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_year_string));
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_month_string));
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_day_string));
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_hour_string));
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_min_string));
					strcat(ADR(Handling.Data.FileName),ADR('_'));
					strcat(ADR(Handling.Data.FileName),ADR(MC_sec_string));
					
				ELSIF TRUE = Fifoglobal.Filewrite_ready THEN (* FifoHandl.st is ready for next filewrite *)  
					IF TRUE = Fifoglobal.Do_filewrite THEN (* FifoHandl.st has read new data from FIFO which is ready to be written to file *)
						Handling.Data.Step := 20; (* Go to file write step *)
						Handling.Data.WriteData := Fifoglobal.Fifo.Fifodata_to_file; (* Transfer data to local variable *) // Todo: Remove intermittent variable Writedata
						Handling.Data.WriteData2 := Fifoglobal.Fifo.Fifodata_to_file2; (* Transfer data to local variable *) // Todo: Remove intermittent variable Writedata
					END_IF	
				END_IF
			
			END_IF
			
			
			IF Handling.Command.bCreateFile = 1 THEN
				
				(* Create a new file with the name from the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 9;  (* next Step*)
				
		
			ELSIF Handling.Command.bWriteFile = 1 THEN
				(* writes the data from variable "Handling.Data.WriteData" 
				   in the file with the name from the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 20;  (* next Step*)  
															
			ELSIF Handling.Command.bDeleteFile = 1 THEN
				(* deletes the file with the name from the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 70;  (* next Step*)  
			END_IF
		
		(***********************************************************************************************)
		9: (* Check the memory status of the device to see if it should be emptied for files soon *) 
			Fifoglobal.Filewrite_ready := 0;
			Handling.Functionblock.DeviceMemory_0.enable := 1;
			Handling.Functionblock.DeviceMemory_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.DeviceMemory_0; (* Call function *) 
			
			IF Handling.Functionblock.DeviceMemory_0.status = 0 THEN  (* DeviceMemory successful *)
				
				IF Handling.Functionblock.DeviceMemory_0.freemem > 20000000 THEN (* If their is free space left (more than 20 MB), then create a new file *)
					MEMORY_IS_FULL_EMPTY_USB := FALSE; 
					Handling.Data.Step := 10;  (* next Step, create new file *)
				ELSE (* If their is no free space left, then do not create a new file *) 
					MEMORY_IS_FULL_EMPTY_USB := TRUE;
				END_IF
				
			ELSIF Handling.Functionblock.DeviceMemory_0.status = ERR_FUB_BUSY THEN  (* DeviceMemory not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.DeviceMemory_0.status;
			END_IF	
		
		10:  (* create a new File with the selected name  *)
			Handling.Functionblock.FileCreate_0.enable := 1;
			Handling.Functionblock.FileCreate_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.FileCreate_0.pFile := ADR(Handling.Data.FileName);  (* name of the file *)
			Handling.Functionblock.FileCreate_0;  (* call the function*)
			
			IF Handling.Functionblock.FileCreate_0.status = 0 THEN  (* FileCreate successful *)
				Handling.Data.Step := 11;  (* next Step*)
				// Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bCreateFile := 0;  (* clear command *)	
			ELSIF Handling.Functionblock.FileCreate_0.status = ERR_FUB_BUSY THEN  (* FileCreate not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileCreate_0.status;
			END_IF	
			
		11:	 (* close file, because of limited number of available file handles on the system *)
			Handling.Functionblock.FileClose_0.enable := 1;
			Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileCreate_0.ident; (* ident for FileCreate-functionblock*)
			Handling.Functionblock.FileClose_0;  (* call the function*)
			
			IF Handling.Functionblock.FileClose_0.status = 0 THEN  (* FileClose successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bCreateFile := 0;  (* clear command *)
				Fifoglobal.Filewrite_ready := 1; (* Report to FifoHandl.st that we are ready to write to file *)
			ELSIF Handling.Functionblock.FileClose_0.status = ERR_FUB_BUSY THEN  (* FileClose not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileClose_0.status;
			END_IF	

		(***********************************************************************************************)	
		20:	(* open file for write access *)
			Handling.Functionblock.FileOpen_0.enable := 1;
			Handling.Functionblock.FileOpen_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.FileOpen_0.pFile :=  ADR(Handling.Data.FileName);  (* name of the file *)
			Handling.Functionblock.FileOpen_0.mode := fiWRITE_ONLY; (* write access *)
			Handling.Functionblock.FileOpen_0;  (* call the function*)

			IF Handling.Functionblock.FileOpen_0.status = 0 THEN  (* FileOpen successful *)
				Handling.Data.Step := 21;  (* next Step*)
			ELSIF Handling.Functionblock.FileOpen_0.status = ERR_FUB_BUSY THEN  (* FileOpen not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileOpen_0.status;
			END_IF	
			
			Fifoglobal.Filewrite_ready := 0;

		21: (* write data into file *)
			Handling.Functionblock.FileWrite_0.enable := 1;
			Handling.Functionblock.FileWrite_0.ident := Handling.Functionblock.FileOpen_0.ident;
			Handling.Functionblock.FileWrite_0.offset := writeoffset;
			Handling.Functionblock.FileWrite_0.pSrc := ADR(Handling.Data.WriteData);
			Handling.Functionblock.FileWrite_0.len := SIZEOF(Handling.Data.WriteData);
			Handling.Functionblock.FileWrite_0;  (* call the function*)

			IF Handling.Functionblock.FileWrite_0.status = 0 THEN  (* FileWrite successful *)
				IF Fifoglobal.extrafilewrite THEN
					Handling.Data.Step := 22;  (* next Step*)
				ELSE
					Handling.Data.Step := 23;  (* next Step*)
				END_IF
				writeoffset := writeoffset + Handling.Functionblock.FileWrite_0.len;
				ELSIF Handling.Functionblock.FileWrite_0.status = ERR_FUB_BUSY THEN  (* FileWrite not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileWrite_0.status;
			END_IF	
			
		22: (* write data into file *)
			Handling.Functionblock.FileWrite_0.enable := 1;
			Handling.Functionblock.FileWrite_0.ident := Handling.Functionblock.FileOpen_0.ident;
			Handling.Functionblock.FileWrite_0.offset := writeoffset;
			Handling.Functionblock.FileWrite_0.pSrc := ADR(Handling.Data.WriteData2);
			Handling.Functionblock.FileWrite_0.len := SIZEOF(Handling.Data.WriteData2);
			Handling.Functionblock.FileWrite_0;  (* call the function*)

			IF Handling.Functionblock.FileWrite_0.status = 0 THEN  (* FileWrite successful *)
				Handling.Data.Step := 23;  (* next Step*)
				writeoffset := writeoffset + Handling.Functionblock.FileWrite_0.len;
			ELSIF Handling.Functionblock.FileWrite_0.status = ERR_FUB_BUSY THEN  (* FileWrite not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileWrite_0.status;
			END_IF	

		23:	 (* close file, because of limited number of available file handles on the system *)
			Handling.Functionblock.FileClose_0.enable := 1;
			Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileOpen_0.ident; (* ident for FileCreate-functionblock*)
		//	Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileCreate_0.ident; (* ident for FileCreate-functionblock*)
			Handling.Functionblock.FileClose_0;  (* call the function*)
			
			IF Handling.Functionblock.FileClose_0.status = 0 THEN  (* FileClose successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bWriteFile := 0;  (* clear command *)
				Fifoglobal.Filewrite_ready := 1; (* We are ready to write a new file *) 
				Fifoglobal.Newfifo := 0; (* Waiting for next FIFO read *) 
				counter := counter + 1;
				
				Fifoglobal.HMI_display := 'Writing to USB';
				
			ELSIF Handling.Functionblock.FileClose_0.status = ERR_FUB_BUSY THEN  (* FileClose not finished -> redo *)			
				(* Busy *)
				Fifoglobal.Filewrite_ready := 0;	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Fifoglobal.Filewrite_ready := 0;
				Error := Handling.Functionblock.FileClose_0.status;
			END_IF	

		(***********************************************************************************************)
		30: (* Check the memory status of the device to see if it should be emptied for files soon *) 
			Fifoglobal.Filewrite_ready := 0;
			Handling.Functionblock.DeviceMemory_0.enable := 1;
			Handling.Functionblock.DeviceMemory_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.DeviceMemory_0; (* Call function *) 
			
			IF Handling.Functionblock.DeviceMemory_0.status = 0 THEN  (* DeviceMemory successful *)
				
				IF Handling.Functionblock.DeviceMemory_0.freemem > 20000000 THEN (* If their is free space left (more than 20 MB), then create a new file *)
					MEMORY_IS_FULL_EMPTY_USB := FALSE; 
					Handling.Data.Step := 31;  (* next Step, create new file *)
				ELSE (* If their is no free space left, then do not create a new file *) 
					MEMORY_IS_FULL_EMPTY_USB := TRUE;
					Fifoglobal.HMI_display := 'USB MEMORY FULL';
				END_IF
				
			ELSIF Handling.Functionblock.DeviceMemory_0.status = ERR_FUB_BUSY THEN  (* DeviceMemory not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.DeviceMemory_0.status;
			END_IF	
		
		31:  (* create a new File with the selected name  *)
			Handling.Functionblock.FileCreate_0.enable := 1;
			Handling.Functionblock.FileCreate_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.FileCreate_0.pFile := ADR(Handling.Data.FileName);  (* name of the file *)
			Handling.Functionblock.FileCreate_0;  (* call the function*)
			
			IF Handling.Functionblock.FileCreate_0.status = 0 THEN  (* FileCreate successful *)
				Handling.Data.Step := 32;  (* next Step*)			
			ELSIF Handling.Functionblock.FileCreate_0.status = ERR_FUB_BUSY THEN  (* FileCreate not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileCreate_0.status;
			END_IF	
		32:	 (* close file, because of limited number of available file handles on the system *)
			Handling.Functionblock.FileClose_0.enable := 1;
			Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileCreate_0.ident; (* ident for FileCreate-functionblock*)
			Handling.Functionblock.FileClose_0;  (* call the function*)
			
			IF Handling.Functionblock.FileClose_0.status = 0 THEN  (* FileClose successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Fifoglobal.Filewrite_ready := 1; (* Report to FifoHandl.st that we are ready to write to file *)
				Handling.Data.Step_zipcase := 33; //33;  (* next Step*)
			ELSIF Handling.Functionblock.FileClose_0.status = ERR_FUB_BUSY THEN  (* FileClose not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileClose_0.status;
			END_IF		
		(***********************************************************************************************)	
		70: (* delete a file *)
		
			Handling.Functionblock.FileDelete_0.enable := 1;
			Handling.Functionblock.FileDelete_0.pDevice := ADR(Handling.Data.Device);  (* name of a linked device *)
			Handling.Functionblock.FileDelete_0.pName := ADR(Handling.Data.FileName);  (* name of the source file *)
			Handling.Functionblock.FileDelete_0;  (* call the function*)
			
			IF Handling.Functionblock.FileDelete_0.status = 0 THEN  (* FileRename successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bDeleteFile := 0;  (* clear command *)
			ELSIF Handling.Functionblock.FileDelete_0.status = ERR_FUB_BUSY THEN  (* FileDelete not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
				Error := Handling.Functionblock.FileDelete_0.status;
			END_IF

		(***********************************************************************************************)	
		255:  (* Here some error Handling has to be implemented *)	
			//	Handling.Data.Step := 1;
			Fifoglobal.HMI_display := 'USB Error ';
			itoa(Error,ADR(Error_string));
			strcat(ADR(Fifoglobal.HMI_display),ADR(Error_string));
			Fifoglobal.blockTestButton3 := 1;
		
			IF EDGEPOS(Fifoglobal.HMI_clear_log) THEN
				Handling.Data.Step := 1;
				Fifoglobal.HMI_clear_log := 0;
				Fifoglobal.blockTestButton3 := 0;
			END_IF
				
		END_CASE
	
	CASE Handling.Data.Step_zipcase OF
		
		 0: (* Idle *)
		
		33:	(* compress file *)
			Handling.Functionblock.FileCompress_0.enable := 1;
			Handling.Functionblock.FileCompress_0.pArchiveDevice := ADR(Handling.Data.Device); (* name of a linked device *)
			Handling.Functionblock.FileCompress_0.pArchiveFile := ADR(Handling.Data.FileName_old_gz);
			Handling.Functionblock.FileCompress_0.pSrcDevice := ADR(Handling.Data.Device);
			Handling.Functionblock.FileCompress_0.pSrcFile := ADR(Handling.Data.FileName_old);
			Handling.Functionblock.FileCompress_0; (* call the function *)
			
			IF Handling.Functionblock.FileCompress_0.status = 0 THEN  (* FileCompress successful *)
				Handling.Data.Step_zipcase := 34;  (* next Step*)
			ELSIF Handling.Functionblock.FileCompress_0.status = ERR_FUB_BUSY THEN  (* FileCompress not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step_zipcase := 255;
			END_IF	
		34: (* delete a file *)
			Handling.Functionblock.FileDelete_0.enable := 1;
			Handling.Functionblock.FileDelete_0.pDevice := ADR(Handling.Data.Device);  (* name of a linked device *)
			Handling.Functionblock.FileDelete_0.pName := ADR(Handling.Data.FileName_old);  (* name of the source file *)
			Handling.Functionblock.FileDelete_0;  (* call the function*)
			
			IF Handling.Functionblock.FileDelete_0.status = 0 THEN  (* FileRename successful *)
				Handling.Data.Step_zipcase := 0;  (* next Step*)
			ELSIF Handling.Functionblock.FileDelete_0.status = ERR_FUB_BUSY THEN  (* FileDelete not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step_zipcase := 255;
			END_IF

	END_CASE
	
END_PROGRAM
