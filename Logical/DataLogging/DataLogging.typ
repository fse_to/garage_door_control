(********************************************************************
 * COPYRIGHT -- Eltronic, DK
 ********************************************************************
 * Package: PWUControl
 * File: PWUControl.typ
 * Author: avi
 * Created: September 22, 2011
 ********************************************************************
 * Data types of package PWUControl
 ********************************************************************)

TYPE
	Fifoglobal_typ : 	STRUCT 
		Fifo : Fifo_typ;
		Filehandling : Filehandling_typ;
		Filewrite_ready : BOOL;
		Do_filewrite : BOOL;
		Newfifo : BOOL;
		reset : BOOL;
		HMI_start_log : BOOL;
		HMI_clear_log : BOOL;
		HMI_display : STRING[30];
		blockTestButton2 : USINT;
		blockTestButton3 : USINT;
		Keep_in_buffer : UDINT;
		buffer_size1 : UDINT;
		buffer_size : UDINT;
		extrafilewrite : BOOL;
		strdata : STRING[1000];
		filename : STRING[80];
	END_STRUCT;
	Fifo_typ : 	STRUCT 
		Fifodata : FifoData_derived;
		Fifodata_to_file2 : FifoData_derived;
		Fifodata_to_file : FifoData_derived;
	END_STRUCT;
	Filehandling_typ : 	STRUCT 
		Device_ready : BOOL;
	END_STRUCT;
	FifoData_derived :ARRAY[0..5199]OF INT; (*Fifo data derived data type, size = n_package*)
END_TYPE
