(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: OfflineControl
 * File: OfflineControl.typ
 * Author: villefrancea
 * Created: May 25, 2016
 ********************************************************************
 * Data types of package OfflineControl
 ********************************************************************)

TYPE
	iCount_typ : 	STRUCT 
		iso4 : SINT;
		iso6 : SINT;
		iso14 : SINT;
	END_STRUCT;
END_TYPE
